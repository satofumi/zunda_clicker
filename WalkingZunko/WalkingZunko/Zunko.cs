﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace WalkingZunko
{
    public partial class Zunko : Form
    {
        const string HappyImage = "WalkingZunko.zzm_wa-i_a.png";
        const string QuestionLeftImage = "WalkingZunko.zzm_hatena_a.png";
        const string QuestionRightImage = "WalkingZunko.zzm_hatena_b.png";
        const string MovingImage = "WalkingZunko.zzm_kirakira.png";

        const int StandingFootOffset = 6;

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int DeleteObject(IntPtr hobject);

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private struct BLENDFUNCTION
        {
            public byte BlendOp;
            public byte BlendFlags;
            public byte SourceConstantAlpha;
            public byte AlphaFormat;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int UpdateLayeredWindow(
            IntPtr hwnd,
            IntPtr hdcDst,
            [System.Runtime.InteropServices.In()]
            ref Point pptDst,
            [System.Runtime.InteropServices.In()]
            ref Size psize,
            IntPtr hdcSrc,
            [System.Runtime.InteropServices.In()]
            ref Point pptSrc,
            int crKey,
            [System.Runtime.InteropServices.In()]
            ref BLENDFUNCTION pblend,
            int dwFlags);

        [DllImport("user32")]
        private static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern int GetWindowRect(IntPtr hwnd, ref RECT lpRect);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        protected override CreateParams CreateParams
        {
            get
            {
                const int WS_EX_LAYERED = 0x00080000;
                CreateParams cp = base.CreateParams;
                cp.ExStyle = cp.ExStyle | WS_EX_LAYERED;
                return cp;
            }
        }

        private void SetLayeredWindow(Bitmap srcBitmap)
        {
            const byte AC_SRC_OVER = 0;
            const byte AC_SRC_ALPHA = 1;
            const int ULW_ALPHA = 2;

            // スクリーンの Graphics と、hdcを取得
            Graphics g_sc = Graphics.FromHwnd(IntPtr.Zero);
            IntPtr hdc_sc = g_sc.GetHdc();

            // BITMAP の Graphicsと、hdc を取得
            Graphics g_bmp = Graphics.FromImage(srcBitmap);
            IntPtr hdc_bmp = g_bmp.GetHdc();

            // BITMAP の hdc でサーフェイスの BITMAP を選択する
            // このとき背景を無色透明にしておく
            IntPtr oldhbmp = SelectObject(hdc_bmp, srcBitmap.GetHbitmap(Color.FromArgb(0)));

            // BLENDFUNCTION を初期化
            BLENDFUNCTION blend = new BLENDFUNCTION();
            blend.BlendOp = AC_SRC_OVER;
            blend.BlendFlags = 0;
            blend.SourceConstantAlpha = 255;
            blend.AlphaFormat = AC_SRC_ALPHA;

            // ウィンドウ位置の設定
            Point pos = new Point(this.Left, this.Top);

            // サーフェースサイズの設定
            Size surfaceSize = new Size(this.Width, this.Height);

            // サーフェース位置の設定
            Point surfacePos = new Point(0, 0);

            // レイヤードウィンドウの設定
            UpdateLayeredWindow(this.Handle, hdc_sc, ref pos, ref surfaceSize, hdc_bmp, ref surfacePos, 0, ref blend, ULW_ALPHA);

            // 後始末
            DeleteObject(SelectObject(hdc_bmp, oldhbmp));
            g_sc.ReleaseHdc(hdc_sc);
            g_sc.Dispose();
            g_bmp.ReleaseHdc(hdc_bmp);
            g_bmp.Dispose();
        }

        private void ShowImage(string path)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Bitmap image = new Bitmap(assembly.GetManifestResourceStream(path));
            SetLayeredWindow(image);
            Refresh();
        }

        public Zunko()
        {
            InitializeComponent();
        }

        private void Zunko_Load(object sender, EventArgs e)
        {
            InitializeForm();
            ChangePosition();
        }

        private void InitializeForm()
        {
            FormBorderStyle = FormBorderStyle.None;
            ShowInTaskbar = false;
            Cursor.Current = Cursors.Default;
            Size = ImageSize(HappyImage);
        }

        private void ChangePosition()
        {
            Point startPoint = StartPoint();
            Rectangle window = WindowRectangle(startPoint);
            if (startPoint.X < 0)
            {
                // ウィンドウが最小化されている場合は終了する
                Application.Exit();
                return;
            }

            PlaceFirstPosition(startPoint);
            DropToBottom(window);
            Searching();
            WalkToSide(startPoint, window);

            Application.Exit();
        }

        private Size ImageSize(string path)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Bitmap image = new Bitmap(assembly.GetManifestResourceStream(path));
            return image.Size;
        }

        private void PlaceFirstPosition(Point startPoint)
        {
            Location = new Point(startPoint.X - (Size.Width / 2), startPoint.Y);
            ShowImage(HappyImage);
        }

        private void DropToBottom(Rectangle window)
        {
            int x = Location.X;
            int y = Location.Y;
            int speed = 4;
            int targetY;
            do
            {
                Location = new Point(x, y);
                WaitSeconds(0.013);
                y += speed;
                targetY = BottomY(Location) - Size.Height + StandingFootOffset;
            } while (y < targetY);

            Location = new Point(x, targetY);
        }

        private void Searching()
        {
            WaitSeconds(1.0);

            double poseSeconds = 0.8;
            for (int i = 0; i < 2; ++i)
            {
                UpdateYPosition(Location);
                ShowImage(QuestionRightImage);
                WaitSeconds(poseSeconds);

                UpdateYPosition(Location);
                ShowImage(QuestionLeftImage);
                WaitSeconds(poseSeconds);
            }

            UpdateYPosition(Location);
            ShowImage(QuestionRightImage);
            WaitSeconds(2.0);
        }
        
        private void UpdateYPosition(Point point)
        {
            int y = BottomY(Location) - Size.Height + StandingFootOffset;
            Location = new Point(Location.X, y);
        }

        private void WalkToSide(Point startPoint, Rectangle window)
        {
            ShowImage(MovingImage);

            int x = Location.X;
            int centerX = x + (Size.Width / 2);
            int speed = 5 * ((Math.Abs(centerX - window.X) < Math.Abs(centerX - (window.X + window.Width))) ? -1 : +1);
            int targetX;
            if (Screen.AllScreens.Length == 1)
            {
                // スクリーンが１枚のときは、画面外まで移動させる
                targetX = window.X + ((speed > 0) ? window.Width + Size.Width : -Size.Width);
            }
            else
            {
                // スクリーンが複数あるときは、画面内の端まで移動させる
                targetX = window.X + ((speed > 0) ? window.Width - Size.Width : 0);
            }
            int upDownHeight = -1;
            do
            {
                x += speed;
                int baseY = BottomY(Location) - Size.Height + StandingFootOffset;
                int y = baseY + upDownHeight;
                upDownHeight = -upDownHeight;

                Location = new Point(x, y);
                WaitSeconds(0.1);
            } while ((x * speed) < (targetX * speed));
        }

        private void WaitSeconds(double second)
        {
            var task = Task.Factory.StartNew(() =>
            {
                Thread.Sleep((int)(1000 * second));
            });
            task.Wait();
        }

        private Point StartPoint()
        {
            Process[] ps = Process.GetProcessesByName("ZundaClicker");
            foreach (Process p in ps)
            {
                if (IsWindowVisible(p.MainWindowHandle))
                {
                    RECT rect = new RECT();
                    GetWindowRect(p.MainWindowHandle, ref rect);
                    return new Point((rect.left + rect.right) / 2, rect.bottom);
                }
            }
            return new Point(Screen.PrimaryScreen.Bounds.Width / 2, 0);
        }

        private Rectangle WindowRectangle(Point point)
        {
            Screen screen = Screen.FromPoint(point);
            return screen.Bounds;
        }

        // タスクトレイ情報を返す
        private int BottomY(Point point)
        {
            IntPtr hWnd = FindWindow("Shell_TrayWnd", null);
            int processId;
            GetWindowThreadProcessId(hWnd, out processId);
            Process p = Process.GetProcessById(processId);

            Rectangle windowRectangle = WindowRectangle(point);

            if (IsWindowVisible(p.MainWindowHandle))
            {
                RECT rect = new RECT();
                GetWindowRect(p.MainWindowHandle, ref rect);
                if ((windowRectangle.Bottom == rect.bottom) && (windowRectangle.Top != rect.top))
                {
                    return rect.top;
                }
            }

            return windowRectangle.Bottom;
        }
    }
}
