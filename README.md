# ずんだ餅クリッカー

東北ずん子たち３姉妹による、ほのぼの癒やし系のずんだ餅クリッカーです。
皆さんがこのアプリを遊んで笑顔になるといいな、と思いながら作りました。
短い時間でクリアできるように調整したので、ちょっとした時間に遊んでみてくださいね。

## どんなゲームなの？

クリックしてずんだ餅を落とし、クリックせずにスキルを発動させる、そんな感じのゲームです。


## ライセンス

MIT License


## 紹介サイト

http://hyakuren-soft.sakura.ne.jp/ZundaClicker/


## 開発サイト

https://bitbucket.org/satofumi/zunda_clicker/


## Author

Satofumi


## 連絡先

バグや要望がありましたら、開発サイトの課題トラッカーかメールアドレスまでお知らせ下さい。

tracker: https://bitbucket.org/satofumi/zunda_clicker/issues
e-mail : satofumi@users.sourceforge.jp


## 素材

 * キャラクタ画像、ずんだ餅
   * 東北ずん子公式 (http://zunko.jp/)
   * 東北ずん子ポータル (http://t-zunko.moe/)

 * 背景画像、マップチップ、アイコン
   * ぴぽや (http://piposozai.blog76.fc2.com/)

 * BGM・効果音
   * スキップモア (http://www.skipmore.com/)

 * ミュートアイコン
   * FatCow (http://www.fatcow.com/free-icons)
