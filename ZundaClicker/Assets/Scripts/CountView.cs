﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CountView : MonoBehaviour
{
    public Text CountLabel;
    public Text VelocityLabel;
    public Text MaxDroppedPerClickLabel;
    public Text DroppedPerClickLabel;

    public void UpdateDroppedCount(int count)
    {
        CountLabel.text = count.ToString("#,0");
    }

    public void UpdateMaxDroppedPerClick(int count)
    {
        MaxDroppedPerClickLabel.text = count.ToString();
    }

    public void UpdateDroppedPerClick(int count)
    {
        DroppedPerClickLabel.text = count.ToString();
    }

    public void UpdateVelocityValue(float velocity)
    {
        VelocityLabel.text = velocity.ToString("F1");
    }
}
