﻿using UnityEngine;
using System.Collections;

public class TitleController : MonoBehaviour
{
    public GameObject ClickToStartText;
    public GameObject ClickedSound;

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            ClickedSound.GetComponent<AudioSource>().Play();

            ClickToStartText.GetComponent<Blinker>().Hide();
            GetComponent<SceneFadeInOut>().EndScene("Clicker");
        }
    }
}
