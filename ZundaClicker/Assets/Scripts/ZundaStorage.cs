﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZundaStorage : MonoBehaviour
{
    private float createCount;
    private List<GameObject> balls;
    private float velocityCount;
    private int cloggedSeconds;
    private int totalDroppedCount;
    private int previousVelocityBallCount;
    private float previousVelocityTiming;
    private int maxDroppedPerClickCount;
    private int droppedPerClickCount;

    public DataStorage storage;
    public CountView view;
    public AchievementManager achievements;

    public Zunko zunko;
    public Itako itako;
    public Kiritan kiritan;

    public GameObject ZundaPrefab;
    public int MaxSize = 80;
    public float CreateInterval = 0.05f;
    public float BallFirstVelocity = 8.0f;
    public int NortifyCloggedSeconds = 3;

    void Start()
    {
        balls = new List<GameObject>();
        previousVelocityTiming = Time.time;
        maxDroppedPerClickCount = 0;
        droppedPerClickCount = 0;
        createCount = 0;
        velocityCount = 0;
        cloggedSeconds = 0;
        IsGenerating = true;

        // DataStorage に登録されていた値を読み込む
        totalDroppedCount = storage.ZundaCount;
        achievements.RegisterCurrentZundaCount(totalDroppedCount);
        previousVelocityBallCount = totalDroppedCount;
        maxDroppedPerClickCount = storage.MaxZundaWithoutClick;

        // ずんだ餅が落下し続けるのを防ぐために、一定量のずんだ餅を発生させる
        GenerateSomeBalls();
    }

    void Update()
    {
        GenerateBallIfPossible();
        UpdateBalls();
    }

    // クリックなしで落ちたずんだ餅の数、のリセット
    public void ResetZundaWithoutClickCount()
    {
        droppedPerClickCount = 0;
    }

    // ずんだ餅配列の最初のずんだ餅の位置を返す
    public Vector3? FirstZundaPosition()
    {
        if (balls.Count <= 0)
        {
            return null;
        }

        return balls[0].transform.position;
    }

    // 地面に落ちてるずんだ餅の位置を返す
    public Vector3? GroundedZundaPosition()
    {
        if (balls.Count <= 0)
        {
            return null;
        }

        Transform frontZunda = balls[0].transform;
        if (frontZunda.GetComponent<Rigidbody2D>().velocity.magnitude > 0.001)
        {
            return null;
        }
        if ((frontZunda.position.x < Movable.GroundRightEdge) && (frontZunda.position.y < Movable.GroundTopEdge))
        {
            frontZunda.GetComponent<Renderer>().sortingOrder = 50;
            return frontZunda.position;
        }

        return null;
    }

    // ずんだ餅配列の最初の位置のずんだ餅を削除する
    public void RemoveFirstZunda(float leftSeconds = 0)
    {
        if (balls.Count <= 0)
        {
            return;
        }

        Destroy(balls[0], leftSeconds);
        balls.RemoveAt(0);
    }

    // 位置と速度を指定してずんだ餅を生成する
    public void CreateZunda(Vector2 position, Vector2 velocity)
    {
        GenerateBallObject(position, velocity);
    }

    // ずんだ餅のカウントを増やす
    public void IncrementZundaCount()
    {
        storage.ZundaCount = ++totalDroppedCount;
        view.UpdateDroppedCount(totalDroppedCount);
    }

    // ずんだ口寄せ中かどうかの設定
    public bool IsKuchiyoseDrop
    {
        set;
        get;
    }

    // きりたん砲の発射直後かどうかの設定
    public bool IsKiritanFire
    {
        set;
        get;
    }

    // ずんだ餅を生成するかどうかの設定
    public bool IsGenerating
    {
        set;
        get;
    }

    // 上限数を超えない範囲でずんだ餅を生成する
    private void GenerateBallIfPossible()
    {
        float deltaTime = Time.deltaTime;

        createCount += deltaTime;
        while (createCount >= CreateInterval)
        {
            GenerateBall();
            createCount -= CreateInterval;
        }

        // ZPS を計算して更新する
        velocityCount += deltaTime;
        if (velocityCount > 1.0f)
        {
            velocityCount -= 1.0f;

            float velocity = UpdateVelocity();
            cloggedSeconds = (velocity == 0) ? (cloggedSeconds + 1) : 0;

            // ずんだ餅が詰まって時間が経過したら各キャラクターのスキル発動を行う
            if (cloggedSeconds >= NortifyCloggedSeconds)
            {
                ActivateCharacterSkill();
            }
        }
    }

    private void ActivateCharacterSkill()
    {
        if (zunko.isActiveAndEnabled)
        {
            zunko.SendMessage("ResolveClogged");
        }
        if (kiritan.isActiveAndEnabled)
        {
            kiritan.SendMessage("ResolveClogged");
        }
        if (itako.isActiveAndEnabled)
        {
            itako.SendMessage("ResolveClogged");
        }
    }

    // ずんだ餅の生成と情報の更新
    private void UpdateBalls()
    {
        int droppedCount = 0;
        for (int i = balls.Count - 1; i >= 0; --i)
        {
            // 落下したずんだ餅のオブジェクトを削除
            GameObject ball = balls[i];
            float x = ball.transform.position.x;
            float y = ball.transform.position.y;
            Rigidbody2D body = ball.GetComponent<Rigidbody2D>();
            
            if (ball.transform.position.y < Movable.DroppedY)
            {
                balls.Remove(ball);
                Destroy(ball);
                ++droppedCount;
            }
            else if (IsKiritanFire && ((x < Movable.WindowLeft) || (x > Movable.WindowRight) || ((y > Movable.WindowTop) && (body.velocity.y > 0))))
            {
                balls.Remove(ball);
                Destroy(ball);

                FallingZunda.Execute(body.velocity.x, body.velocity.y, ball.transform.position.x, ball.transform.position.y);
            }
        }

        // ずんだ餅が落下したら、各情報を更新する
        if (droppedCount != 0)
        {
            // 落下したずんだ餅の数
            totalDroppedCount += droppedCount;
            storage.ZundaCount = totalDroppedCount;
            view.UpdateDroppedCount(totalDroppedCount);

            // 現在のずんだ餅の数を AchievementsManager に登録する
            achievements.RegisterCurrentZundaCount(totalDroppedCount);

            // クリックせずに落ちたずんだ餅の数
            droppedPerClickCount += droppedCount;
            view.UpdateDroppedPerClick(droppedPerClickCount);
            if (droppedPerClickCount > maxDroppedPerClickCount)
            {
                maxDroppedPerClickCount = droppedPerClickCount;
                storage.MaxZundaWithoutClick = maxDroppedPerClickCount;
                view.UpdateMaxDroppedPerClick(maxDroppedPerClickCount);
            }
        }
    }

    // ずんだ餅の生成
    private void GenerateBall()
    {
        if (!IsGenerating)
        {
            return;
        }

        // 個数が上限を超えないようにする
        if (balls.Count >= MaxSize)
        {
            return;
        }

        float radian = (IsKuchiyoseDrop ? Random.Range(268, 272) : Random.Range(180, 359)) * Mathf.Deg2Rad;
        Vector2 velocity = new Vector2(Mathf.Cos(radian), Mathf.Sin(radian)) * BallFirstVelocity;
        GenerateBallObject(new Vector2(0, 7.3f), velocity);
    }

    // 位置と速度を指定して、実際にずんだ餅を生成する
    private void GenerateBallObject(Vector2 position, Vector2 velocity)
    {
        GameObject ball = (GameObject)Instantiate(ZundaPrefab, position, Quaternion.identity);
        Rigidbody2D rigidbody = ball.GetComponent<Rigidbody2D>();
        rigidbody.velocity = velocity;
        balls.Add(ball);
    }

    // ずんだ餅をまとめて生成する
    private void GenerateSomeBalls()
    {
        for (int i = 0; i < MaxSize / 2; ++i)
        {
            GenerateBall();
        }
    }

    // ZPS 情報の更新
    private float UpdateVelocity()
    {
        float time = Time.time;
        float spentTime = time - previousVelocityTiming;
        float diff = totalDroppedCount - previousVelocityBallCount;
        float velocity = diff / spentTime;
        view.UpdateVelocityValue(velocity);

        previousVelocityTiming = time;
        previousVelocityBallCount = totalDroppedCount;

        return velocity;
    }
}
