﻿using UnityEngine;
using System.Collections;

public class MuteButton : MonoBehaviour
{
    public DataStorage storage;
    public GameObject MuteIcon;

    void Start()
    {
        bool isPlaying = storage.IsSoundPlaying;
        setOnOff(isPlaying);
    }

    public void MuteButtonPressed()
    {
        AudioListener.volume = 1 - AudioListener.volume;
        bool isPlaying = (AudioListener.volume < 0.5) ? false : true;
        storage.IsSoundPlaying = isPlaying;
        storage.Save();
        setOnOff(isPlaying);
    }

    private void setOnOff(bool isPlaying)
    {
        Animator animator = MuteIcon.GetComponent<Animator>();
        animator.SetBool("on", isPlaying);
        AudioListener.volume = isPlaying ? 1 : 0;
    }
}
