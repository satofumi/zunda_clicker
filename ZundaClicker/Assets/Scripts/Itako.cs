﻿using UnityEngine;
using System.Collections;

public class Itako : MonoBehaviour
{
    private enum View : int
    {
        Stand = 0,
        Surprised = 1,
        Rest = 2,
        Sleeping = 3,
    }

    private enum State : int
    {
        Stand,
        Move,
        Sleeping,
        Rest,
        Surprised,
        Summon,
    }

    private static readonly int orderOffset = 1;

    private Animator animator;
    private Renderer characterRenderer;
    private State state;
    private bool isProcessing;
    private AudioSource audioSource;

    public GameController gameController;
    public SkillManager skills;
    public ZundaStorage zunda;

    public GameObject ZundaHole;
    public GameObject Eraser;
    public GameObject SummonCircle;
    public GameObject CutinSummon;

    public AudioClip Hello;
    public AudioClip Happy;
    public AudioClip RestStart;
    public AudioClip SleepStart;
    public AudioClip SkillStart1;
    public AudioClip SkillStart2;


    public bool IsZundaKuchiyoseEnabled
    {
        set;
        get;
    }

    public bool IsCutinEnabled
    {
        set;
        get;
    }

    public bool IsPowerful
    {
        set;
        get;
    }

    public void ResolveClogged()
    {
        if (state != State.Stand)
        {
            return;
        }

        if ((Random.value > 0.5) && IsZundaKuchiyoseEnabled && !skills.IsCooldown(SkillManager.Skill.Itako))
        {
            if (skills.LockSkillActivation())
            {
                // ずんだ口寄せを発動する
                state = State.Summon;
            }
        }
    }

    public void BecomFun()
    {
        if (state == State.Stand)
        {
            state = State.Surprised;
        }
    }

    void OnDisable()
    {
        skills.SetCooldownVisible(SkillManager.Skill.Itako, false);
    }

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        characterRenderer = GetComponent<Renderer>();
        state = State.Move;
    }

    void Update()
    {
        UpdateState();
    }

    private void ChangeView(View value)
    {
        animator.SetInteger("state", (int)value);
    }

    private void ChangeOrder(int order)
    {
        characterRenderer.sortingOrder = orderOffset + order;
    }

    private void UpdateState()
    {
        if (isProcessing)
        {
            return;
        }

        isProcessing = true;
        switch (state)
        {
            case State.Stand:
                StartCoroutine(ProcessStand());
                break;

            case State.Move:
                StartCoroutine(ProcessMove());
                break;

            case State.Sleeping:
                StartCoroutine(ProcessSleeping());
                break;

            case State.Rest:
                StartCoroutine(ProcessRest());
                break;

            case State.Summon:
                StartCoroutine(ProcessSummon());
                break;

            case State.Surprised:
                StartCoroutine(ProcessSurprised());
                break;
        }
    }

    private void ChangeToNextState()
    {
        if (state != State.Stand)
        {
            // コルーチン処理中に ResolveClogged() でスキル使用により state が変更されている場合には戻る
            return;
        }

        // 他の State へランダムに遷移させる
        float randomValue = Random.value;
        if (randomValue > 0.99)
        {
            state = State.Sleeping;
        }
        else if (randomValue > 0.97f)
        {
            state = State.Rest;
        }
        else if (randomValue > 0.95f)
        {
            state = State.Move;
        }
    }

    private IEnumerator ProcessStand()
    {
        ChangeView(View.Stand);
        ChangeOrder(Character.Middle);

        Vector3 upDownWidth = new Vector3(0, 0.015f, 0);
        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, 0.25f, 1));

        ChangeToNextState();
        isProcessing = false;
    }

    private IEnumerator ProcessMove()
    {
        ChangeView(View.Stand);
        ChangeOrder(Character.Back);

        Vector3 upDownWidth = new Vector3(0, 0.015f, 0);
        yield return StartCoroutine(Character.MoveTo(transform, Character.RandomPositionX(), 0.035f, upDownWidth));

        state = State.Stand;
        isProcessing = false;
    }

    private IEnumerator ProcessSleeping()
    {
        audioSource.clip = SleepStart;
        audioSource.Play();

        ChangeView(View.Sleeping);
        ChangeOrder(Character.Front);

        yield return StartCoroutine(Character.Sleep(this, transform));

        state = State.Stand;
        isProcessing = false;
    }

    private IEnumerator ProcessRest()
    {
        audioSource.clip = RestStart;
        audioSource.Play();

        ChangeView(View.Rest);
        ChangeOrder(Character.Front);
        yield return StartCoroutine(Character.Rest(this, transform));

        state = State.Stand;
        isProcessing = false;
    }

    private IEnumerator ProcessSurprised()
    {
        audioSource.clip = Happy;
        audioSource.Play();

        ChangeView(View.Surprised);
        ChangeOrder(Character.Front);

        Vector3 upDownWidth = new Vector3(0, 0.04f, 0);
        float intervalSecond = 0.2f;
        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, intervalSecond, (int)(Character.SurprisedSeconds / intervalSecond)));

        state = State.Stand;
        isProcessing = false;
    }

    // 「ずんだ口寄せ」
    private IEnumerator ProcessSummon()
    {
        audioSource.clip = (Random.value > 0.5f) ? SkillStart1 : SkillStart2;
        audioSource.Play();

        ChangeView(View.Surprised);
        ChangeOrder(Character.MostFront);

        yield return new WaitForSeconds(1.0f);

        Vector3 upDownWidth = new Vector3(0, 0.20f, 0);
        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, 0.25f, 2));

        GameObject summonCircle = IsPowerful ? (GameObject)Instantiate(SummonCircle, new Vector3(transform.position.x - 0.06f, -3.75f, 0), Quaternion.identity) : null;

        yield return new WaitForSeconds(1.0f);

        if (IsCutinEnabled)
        {
            yield return StartCoroutine(Character.PlayCutin(this, CutinSummon));
        }

        yield return new WaitForSeconds(0.5f);

        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, 0.25f, 2));
        yield return StartCoroutine(SummonZunda());

        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, 0.25f, 1));

        Destroy(summonCircle);
        yield return new WaitForSeconds(1.5f);

        ChangeView(View.Stand);
        yield return new WaitForSeconds(1.5f);

        skills.UnlockSkillActivation();
        skills.ResetCooldownCount(SkillManager.Skill.Itako);
        state = State.Sleeping;
        isProcessing = false;
    }

    private IEnumerator SummonZunda()
    {
        zunda.IsKuchiyoseDrop = true;
        zunda.IsGenerating = false;

        int summonTimes = IsPowerful ? 60 : 12;
        for (int i = 0; i < summonTimes; ++i)
        {
            Vector3? position = zunda.FirstZundaPosition();
            if (!position.HasValue)
            {
                break;
            }
            if ((position.Value.y > -3.0f) || (Mathf.Abs(position.Value.x) > 1.0f))
            {
                GameObject eraser = (GameObject)Instantiate(Eraser, position.Value, Quaternion.identity);
                Destroy(eraser, 0.39f);
            }  
            zunda.RemoveFirstZunda(0.30f);
            
            yield return new WaitForSeconds(0.3f);
            gameController.SendMessage("ResolveCloggedByCharacter");
        }

        Vector3 upDownWidth = new Vector3(0, 0.20f, 0);
        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, 0.25f, 2));

        GameObject zundaHole = (GameObject)Instantiate(ZundaHole, new Vector3(0, 4.85f, 0), Quaternion.identity);
        yield return new WaitForSeconds(1.0f);

        zunda.IsGenerating = true;

        int fallSeconds = IsPowerful ? 12 : 6;
        for (int i = 0; i < fallSeconds; ++i)
        {
            gameController.SendMessage("ResolveCloggedByCharacter");
            yield return new WaitForSeconds(1.0f);
        }

        zunda.IsKuchiyoseDrop = false;
        yield return new WaitForSeconds(0.4f);
        Destroy(zundaHole);
    }

    public void PlayHello()
    {
        audioSource.clip = Hello;
        audioSource.Play();
    }
}
