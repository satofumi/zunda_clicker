﻿using UnityEngine;
using System.Collections;

public class Kiritan : MonoBehaviour
{
    private enum View : int
    {
        Stand = 0,
        Sleeping = 1,
        Fire = 2,
    };

    private enum State : int
    {
        Stand,
        Move,
        Sleeping,
        Surprised,
        Fire,
    }

    private static readonly int orderOffset = 2;

    private Animator animator;
    private Renderer characterRenderer;
    private State state;
    private bool isProcessing;
    private AudioSource audioSource;

    public GameController gameController;
    public SkillManager skills;
    public ZundaStorage zunda;
    public GameObject Detonator;
    public GameObject Shell;
    public GameObject CutinFire;
    public GameObject MisoBomb;

    public AudioClip Hello;
    public AudioClip Happy;
    public AudioClip SleepStart;
    public AudioClip FireSkill1;
    public AudioClip FireSkill2;


    public bool IsKiritanHouEnabled
    {
        set;
        get;
    }

    public bool IsCutinEnabled
    {
        set;
        get;
    }

    public bool IsPowerful
    {
        set;
        get;
    }

    public void ResolveClogged()
    {
        if (state != State.Stand)
        {
            return;
        }

        if ((Random.value > 0.5) && IsKiritanHouEnabled && !skills.IsCooldown(SkillManager.Skill.Kiritan))
        {
            if (skills.LockSkillActivation())
            {
                // きりたん砲を発動する
                state = State.Fire;
            }
        }
    }

    public void BecomFun()
    {
        if (state == State.Stand)
        {
            state = State.Surprised;
        }
    }

    void OnDisable()
    {
        skills.SetCooldownVisible(SkillManager.Skill.Kiritan, false);
    }

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        characterRenderer = GetComponent<Renderer>();
        state = State.Move;
    }

    void Update()
    {
        UpdateState();
    }

    private void ChangeView(View value)
    {
        animator.SetInteger("state", (int)value);
    }

    private void ChangeOrder(int order)
    {
        characterRenderer.sortingOrder = orderOffset + order;
    }

    private void UpdateState()
    {
        if (isProcessing)
        {
            return;
        }

        isProcessing = true;
        switch (state)
        {
            case State.Stand:
                StartCoroutine(ProcessStand());
                break;

            case State.Move:
                StartCoroutine(ProcessMove());
                break;

            case State.Sleeping:
                StartCoroutine(ProcessSleeping());
                break;

            case State.Fire:
                StartCoroutine(ProcessFire());
                break;

            case State.Surprised:
                StartCoroutine(ProcessSurprised());
                break;
        }
    }

    private void ChangeToNextState()
    {
        if (state != State.Stand)
        {
            // コルーチン処理中に ResolveClogged() でスキル使用により state が変更されている場合には戻る
            return;
        }

        // 他の State へランダムに遷移させる
        float randomValue = Random.value;
        if (randomValue > 0.99)
        {
            state = State.Sleeping;
        }
        else if (randomValue > 0.97f)
        {
            state = State.Move;
        }
    }

    private IEnumerator ProcessStand()
    {
        ChangeView(View.Stand);
        ChangeOrder(Character.Middle);

        Vector3 upDownWidth = new Vector3(0, 0.02f, 0);
        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, 0.2f, 1));

        ChangeToNextState();
        isProcessing = false;
    }

    private IEnumerator ProcessMove()
    {
        ChangeView(View.Stand);
        ChangeOrder(Character.Back);

        Vector3 upDownWidth = new Vector3(0, 0.02f, 0);
        yield return StartCoroutine(Character.MoveTo(transform, Character.RandomPositionX(), 0.067f, upDownWidth));

        state = State.Stand;
        isProcessing = false;
    }

    private IEnumerator ProcessSleeping()
    {
        audioSource.clip = SleepStart;
        audioSource.Play();

        ChangeView(View.Sleeping);
        ChangeOrder(Character.Front);

        yield return StartCoroutine(Character.Sleep(this, transform));

        state = State.Stand;
        isProcessing = false;
    }

    private IEnumerator ProcessSurprised()
    {
        audioSource.clip = Happy;
        audioSource.Play();

        ChangeOrder(Character.Front);

        Vector3 upDownWidth = new Vector3(0, 0.2f, 0);
        float intervalSecond = 0.2f;
        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, intervalSecond, (int)(Character.SurprisedSeconds / intervalSecond)));

        state = State.Stand;
        isProcessing = false;
    }

    private IEnumerator ProcessFire()
    {
        ChangeOrder(Character.Back);

        Vector3 upDownWidth = new Vector3(0, 0.02f, 0);
        yield return StartCoroutine(Character.MoveTo(transform, Movable.KiritanFirePosition, 0.1f, upDownWidth));

        audioSource.clip = (Random.value > 0.5f) ? FireSkill1 : FireSkill2;
        audioSource.Play();

        yield return new WaitForSeconds(1.0f);

        ChangeView(View.Fire);
        ChangeOrder(Character.MostFront);
        yield return new WaitForSeconds(0.75f);

        if (IsCutinEnabled)
        {
            yield return StartCoroutine(Character.PlayCutin(this, CutinFire));
        }

        yield return new WaitForSeconds(0.5f);
        zunda.IsKiritanFire = true;
        Fire();
        yield return StartCoroutine(ShakeScreen());
        yield return new WaitForSeconds(1.0f);
        zunda.IsKiritanFire = false;
        yield return new WaitForSeconds(1.0f);

        ChangeView(View.Stand);
        yield return new WaitForSeconds(1.0f);

        skills.UnlockSkillActivation();
        skills.ResetCooldownCount(SkillManager.Skill.Kiritan);
        state = State.Move;
        isProcessing = false;
    }

    private void Fire()
    {
        if (IsPowerful)
        {
            // 強めの爆発エフェクト
            GameObject explosion = (GameObject)Instantiate(Detonator, new Vector3(-0.2f, -2.0f, 0), Quaternion.identity);
            Destroy(explosion, 11);

            // 効果音再生
            explosion.GetComponent<AudioSource>().Play();
        }
        else
        {
            // 弱めの爆発エフェクト
            GameObject explosion = (GameObject)Instantiate(MisoBomb, new Vector3(-0.40f, -2.5f, 0), Quaternion.identity);
            Destroy(explosion, 1);
        }

        // きりたん砲の弾の発射
        GameObject shell = (GameObject)Instantiate(Shell, new Vector3(-0.1f, -2.2f, 0), Quaternion.identity);
        Destroy(shell, 10);

        if (!IsPowerful)
        {
            // 弱めの爆発のときの効果音再生
            shell.GetComponent<AudioSource>().Play();
        }

        Rigidbody2D shellBody = shell.GetComponent<Rigidbody2D>();
        float radian = 80 * Mathf.Deg2Rad;
        float BallFirstVelocity = IsPowerful ? 30 : 8;
        shellBody.velocity = new Vector2(Mathf.Cos(radian), Mathf.Sin(radian)) * BallFirstVelocity;

        gameController.SendMessage("ResolveCloggedByCharacter");
    }

    private IEnumerator ShakeScreen()
    {
        Vector3 shakeWidth = new Vector3(0, 0.10f, 0);
        float tinyTime = 0.03f;

        for (int i = 0; i < 5; ++i)
        {
            Camera.main.transform.position += shakeWidth;
            yield return new WaitForSeconds(tinyTime);

            Camera.main.transform.position -= shakeWidth;
            yield return new WaitForSeconds(tinyTime);
        }
    }

    public void PlayHello()
    {
        audioSource.clip = Hello;
        audioSource.Play();
    }
}
