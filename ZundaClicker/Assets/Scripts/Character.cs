﻿using UnityEngine;
using System.Collections;

public class Character
{
    public static readonly float SurprisedSeconds = 1.5f;

    public static readonly int MostFront = 20;
    public static readonly int Front = 15;
    public static readonly int Middle = 10;
    public static readonly int Back = 5;

    // 指定場所まで移動させる
    public static IEnumerator MoveTo(Transform transform, float targetX, float velocityX, Vector3 shakeWidth)
    {
        float xx = Mathf.Sign(targetX - transform.position.x);
        Vector3 velocity = new Vector3(velocityX * xx, 0, 0);

        while (xx == Mathf.Sign(targetX - transform.position.x))
        {
            yield return new WaitForSeconds(0.1f);
            transform.position += velocity + shakeWidth;

            yield return new WaitForSeconds(0.1f);
            transform.position += velocity - shakeWidth;
        }
    }

    // 眠らせる
    public static IEnumerator Sleep(MonoBehaviour obj, Transform transform)
    {
        Vector3 leftRightWidth = new Vector3(0.015f, 0, 0);
        do {
            yield return new WaitForSeconds(Random.Range(4f, 8f));
            yield return obj.StartCoroutine(Character.ShakeLoop(transform, leftRightWidth, 0.4f, 2));
        } while (Random.value < 0.65f);
    }

    public static IEnumerator Rest(MonoBehaviour obj, Transform transform)
    {
        Vector3 leftRightWidth = new Vector3(0.02f, 0, 0);
        do {
            yield return new WaitForSeconds(Random.Range(3f, 5f));
            yield return obj.StartCoroutine(Character.ShakeLoop(transform, leftRightWidth, 0.6f, 1));
        } while (Random.value < 0.5f);
    }

    // 微動させる
    public static IEnumerator ShakeLoop(Transform transform, Vector3 shakeWidth, float waitSeconds, int loopTimes, bool addOnly = false)
    {
        for (int i = 0; i < loopTimes; ++i)
        {
            transform.position += shakeWidth;
            yield return new WaitForSeconds(waitSeconds);

            if (!addOnly)
            {
                transform.position -= shakeWidth;
                yield return new WaitForSeconds(waitSeconds);
            }
        }
    }

    // 移動可能な範囲内でランダムな値を返す
    public static float RandomPositionX()
    {
        return (Random.value > 0.5f) ? Random.Range(Movable.LeftMin, Movable.LeftMax) : Random.Range(Movable.RightMin, Movable.RightMax);
    }

    public static IEnumerator PlayCutin(MonoBehaviour obj, GameObject prefab)
    {
        GameObject cutin = (GameObject)Object.Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity);
        Cutin controller = cutin.GetComponent<Cutin>();
        yield return obj.StartCoroutine(controller.Play());
        Object.Destroy(cutin);
    }
}
