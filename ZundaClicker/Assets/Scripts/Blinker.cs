﻿using UnityEngine;
using System.Collections;

public class Blinker : MonoBehaviour
{
    public GameObject Target;
    public float BlinkIntervalSeconds;

    private bool isBlinking = true;

    void Start()
    {
        Show();
    }

    private IEnumerator Blink()
    {
        float waitSeconds = BlinkIntervalSeconds / 2.0f;
        while (isBlinking)
        {
            Target.SetActive(true);
            yield return new WaitForSeconds(waitSeconds);

            Target.SetActive(false);
            yield return new WaitForSeconds(waitSeconds);
        }
    }

    public void Show()
    {
        StartCoroutine(Blink());
    }

    public void Hide()
    {
        isBlinking = false;
        Target.SetActive(false);
    }
}
