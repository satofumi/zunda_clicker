﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class DataStorage : MonoBehaviour
{
    private string dataPath;
    private SaveData saveData;

    [Serializable]
    private class SaveData
    {
        public int ZundaCount;
        public int MaxZundaWithoutClick;
        public int UnlockedZundaCount;
        public string RegisterKey;
        public bool IsSoundPlaying;
    }

    void Awake()
    {
        dataPath = Application.persistentDataPath + "/save.dat";

#if UNITY_IPHONE
        Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
#endif

        Load();
    }

    private void Load()
    {
        try
        {
            using (FileStream file = new FileStream(dataPath, FileMode.Open, FileAccess.Read))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                saveData = formatter.Deserialize(file) as SaveData;
            }
        }
        catch (Exception)
        {
            saveData = new SaveData();
        }
    }

    public void Save()
    {
        if (saveData == null)
        {
            return;
        }

        using (FileStream stream = new FileStream(dataPath, FileMode.Create, FileAccess.Write))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, saveData);
        }
    }

    public void ResetData()
    {
        saveData.ZundaCount = 0;
        saveData.MaxZundaWithoutClick = 0;
        saveData.UnlockedZundaCount = 0;
    }

    public void ClearAllData()
    {
        saveData.RegisterKey = "";
    }

    public string RegisterKey
    {
        set
        {
            saveData.RegisterKey = value;
        }

        get
        {
            return saveData.RegisterKey;
        }
    }

    public int ZundaCount
    {
        set
        {
            saveData.ZundaCount = value;
        }

        get
        {
            return saveData.ZundaCount;
        }
    }

    public int MaxZundaWithoutClick
    {
        set
        {
            saveData.MaxZundaWithoutClick = value;
        }

        get
        {
            return saveData.MaxZundaWithoutClick;
        }
    }

    public int UnlockedZundaCount
    {
        set
        {
            saveData.UnlockedZundaCount = value;
        }

        get
        {
            return saveData.UnlockedZundaCount;
        }
    }

    public bool IsSoundPlaying
    {
        set
        {
            saveData.IsSoundPlaying = value;
        }

        get
        {
            return saveData.IsSoundPlaying;
        }
    }
}
