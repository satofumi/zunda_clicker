﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour
{
    private bool isShaking;

    public DataStorage storage;
    public AchievementManager achievements;
    public SkillManager skills;
    public CountView view;
    public ZundaStorage zunda;

    public GameObject BarLeft;
    public GameObject BarRight;

    public Text CutinText;
    public GameObject CutinForInitialize;

    public float ShakeValue;
    public float ShakeGravityThreshold;

    void Start()
    {
        CutinForInitialize.GetComponent<Cutin>().SetCutinLabel(CutinText);
        view.UpdateDroppedCount(storage.ZundaCount);
        view.UpdateMaxDroppedPerClick(storage.MaxZundaWithoutClick);

        achievements.SetFader(GetComponent<SceneFadeInOut>());
    }

    void Update()
    {
        HandleInput();
    }

    void OnApplicationPause()
    {
        storage.Save();
    }

    void OnApplicationQuit()
    {
        storage.Save();
    }

    public void ResolveCloggedByCharacter()
    {
        // スキルによるずんだ餅の詰まり解除をクリックとみなして処理する
        zunda.ResetZundaWithoutClickCount();
    }

    public void ChangeTo(string sceneName)
    {
        storage.Save();
        GetComponent<SceneFadeInOut>().EndScene(sceneName);
    }

    private void HandleInput()
    {
        // 加速度の変更をクリックとみなす
        if (Application.isMobilePlatform)
        {
            if (Mathf.Abs(1.0f - Input.acceleration.magnitude) > ShakeGravityThreshold)
            {
                Shake();
            }
        }

        // クリック操作の検出
        if (Input.GetButtonDown("Fire1"))
        {
            Shake();
        }
    }

    private void Shake()
    {
        if (isShaking)
        {
            return;
        }

        // クリックでスキルのクールダウンを減らす
        skills.DecrementCooldowns();
        StartCoroutine(ShakeUpDown());
    }

    private IEnumerator ShakeUpDown()
    {
        isShaking = true;

        ShakeBars(-ShakeValue);
        float tinyTimeout = 0.1f;
        yield return new WaitForSeconds(tinyTimeout);
        ShakeBars(ShakeValue);

        zunda.ResetZundaWithoutClickCount();
        view.UpdateDroppedPerClick(0);

        isShaking = false;
    }

    private void ShakeBars(float shakeValue)
    {
        Move(BarLeft, shakeValue);
        Move(BarRight, shakeValue);
    }

    private void Move(GameObject bar, float shakeValue)
    {
        Rigidbody2D rigidbody = bar.GetComponent<Rigidbody2D>();
        rigidbody.position += Vector2.up * shakeValue;
    }
}

    
