﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TextPlotter : MonoBehaviour
{
    public float PlotIntervalSec = 0.1f;
    public float LineEndWaitSec = 1.0f;
    public int ShowLines = 6;
    public AudioSource PlotSound;

    private Text text;
    private string[] originalLines;
    private List<string> displayLines;
    private int lineIndex;
    private int characterIndex;
    private float previousTime;
    private bool isEnded;

    void Start()
    {
        text = GetComponent<Text>();
        originalLines = text.text.Split(new char[] { '\n' });

        displayLines = new List<string>();
        for (int i = 0; i < ShowLines; ++i)
        {
            displayLines.Add("");
        }

        isEnded = false;
        lineIndex = 0;
        characterIndex = 0;
        UpdateText();

        previousTime = Time.time;
    }

    void Update()
    {
        if (isEnded)
        {
            return;
        }

        float now = Time.time;
        if ((now - previousTime) > PlotIntervalSec)
        {
            previousTime += PlotIntervalSec;
            UpdateText();
        }
    }

    private void UpdateText()
    {
        if (characterIndex >= originalLines[lineIndex].Length)
        {
            UpdateLine();
            return;
        }
        string ch = originalLines[lineIndex].Substring(characterIndex, 1);
        displayLines[ShowLines - 1] += ch;
        if ((ch != " ") && (ch != "\n") && (ch != "\r"))
        {
            PlotSound.Play();
        }
        ++characterIndex;

        text.text = string.Join("\n", displayLines.ToArray());
    }

    private void UpdateLine()
    {
        ++lineIndex;
        if (lineIndex >= originalLines.Length)
        {
            isEnded = true;
            return;
        }
        characterIndex = 0;
        displayLines.RemoveAt(0);
        displayLines.Add("");

        previousTime += LineEndWaitSec;
    }
}
