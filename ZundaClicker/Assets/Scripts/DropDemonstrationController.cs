﻿using UnityEngine;
using System.Collections;

public class DropDemonstrationController : MonoBehaviour
{
    public GameObject Zunda;
    public int MaxZundaCount = 8000;

    private int zundaCount;

    void Start()
    {
        zundaCount = 0;
    }

    void Update()
    {
        if (zundaCount < MaxZundaCount)
        {
            addZunda();
        }
    }

    private void addZunda()
    {
        float tinyX = Random.Range(-0.2f, +0.2f);
        Instantiate(Zunda, new Vector3(tinyX, 3, 0), Quaternion.identity);
        ++zundaCount;
    }
}
