﻿using UnityEngine;
using System.Collections;

public class GameCompleteController : MonoBehaviour
{
    private SceneFadeInOut fader;

    void Start()
    {
        fader = GetComponent<SceneFadeInOut>();
        fader.FadeColor = Color.white;
        fader.FadeSpeed = 1.5f;
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            fader.EndScene("Clicker");
        }
    }
}
