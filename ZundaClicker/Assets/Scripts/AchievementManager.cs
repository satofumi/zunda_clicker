﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AchievementManager : MonoBehaviour
{
    private static bool isFadingShowBackground = false;
    private static bool isLoading = true;
    private static bool isGameCompleted = false;

    private int unlockedIndex = -1;
    private int lastRegisteredZundaCount = 0;
    private SceneFadeInOut sceneFader;

    private delegate void Execute();

    private class Event
    {
        public int UnlockZundaCount
        {
            set;
            get;
        }

        public string AbstractText
        {
            set;
            get;
        }

        public Execute Action;

        public Event(int unlockZundaCount, Execute action, string abstractText)
        {
            UnlockZundaCount = unlockZundaCount;
            AbstractText = abstractText;
            Action = action;
        }
    }

    private static readonly Event[] Events = new Event[] 
    {
        new Event(100, ShowBackground, " 背景画像"),
        new Event(250, ShowZunko, " 東北ずん子"),
        new Event(400, ShowKiritan, " 東北きりたん"),
        new Event(550, ShowItako, " 東北イタコ"),

        new Event(700, EnableKiritanHou, "「きりたん砲」(きりたん)"),
        new Event(850, EnableZundaKuchiyose, "「ずんだ口寄せ」(イタコ)"),
        new Event(1000, EnableZundaArrow, "「ずんだアロー」(ずん子)"),

        new Event(1200, ShowCutin, " カットイン演出"),
        new Event(1400, EnableJumpDrop, "「もちもちジャンプ」(ずん子)"),

        new Event(2000, PowerupZundaKuchiyose, "「ずんだ口寄せ」PowerUP"),
        new Event(2400, PowerupZundaArrow, "「ずんだアロー」PowerUP"),
        new Event(2800, PowerupKiritanHou, "「きりたん砲」PowerUp"),
        new Event(3200, AddEatingCutin, "「餅カットイン」PowerUp"),

        new Event(5000, AddGraphButton, " Game Complete"),
    };

    private static void ShowBackground()
    {
        if (isLoading)
        {
            ChangeBackground();
        }
        else
        {
            isFadingShowBackground = true;
        }
    }

    private static void ChangeBackground()
    {
        BackgroundStatic.SetActive(true);
        AdditionalInformationStatic.SetActive(true);
    }

    private static void ShowZunko()
    {
        if (!isLoading)
        {
            ZunkoStatic.transform.position = Movable.ScreenoutLeft;
        }
        ZunkoStatic.SetActive(true);
        ZunkoStatic.GetComponent<Zunko>().PlayHello();

        EnableEating();
    }

    private static void ShowKiritan()
    {
        if (!isLoading)
        {
            KiritanStatic.transform.position = Movable.ScreenoutLeft;
        }
        KiritanStatic.SetActive(true);
        KiritanStatic.GetComponent<Kiritan>().PlayHello();
    }

    private static void ShowItako()
    {
        if (!isLoading)
        {
            ItakoStatic.transform.position = Movable.ScreenoutLeft;
        }
        ItakoStatic.SetActive(true);
        ItakoStatic.GetComponent<Itako>().PlayHello();
    }

    private static void EnableEating()
    {
        ZunkoStatic.GetComponent<Zunko>().IsEatingEnabled = true;
        ZunkoStatic.GetComponent<Zunko>().BecomFun();
    }

    private static void EnableKiritanHou()
    {
        KiritanStatic.GetComponent<Kiritan>().IsKiritanHouEnabled = true;
        skillsStatic.SetCooldownVisible(SkillManager.Skill.Kiritan, true, !isLoading);
        if (!isLoading)
        {
            skillsStatic.ClearCooldownCount(SkillManager.Skill.Kiritan);
        }
        KiritanStatic.GetComponent<Kiritan>().BecomFun();
    }

    private static void EnableZundaKuchiyose()
    {
        ItakoStatic.GetComponent<Itako>().IsZundaKuchiyoseEnabled = true;
        skillsStatic.SetCooldownVisible(SkillManager.Skill.Itako, true, !isLoading);
        if (!isLoading)
        {
            skillsStatic.ClearCooldownCount(SkillManager.Skill.Itako);
        }
        ItakoStatic.GetComponent<Itako>().BecomFun();
    }

    private static void EnableZundaArrow()
    {
        ZunkoStatic.GetComponent<Zunko>().IsZundaArrowEnabled = true;
        skillsStatic.SetCooldownVisible(SkillManager.Skill.Arrow, true, !isLoading);
        if (!isLoading)
        {
            skillsStatic.ClearCooldownCount(SkillManager.Skill.Arrow);
        }
        ZunkoStatic.GetComponent<Zunko>().BecomFun();
    }

    private static void ShowCutin()
    {
        ZunkoStatic.GetComponent<Zunko>().IsCutinEnabled = true;
        KiritanStatic.GetComponent<Kiritan>().IsCutinEnabled = true;
        ItakoStatic.GetComponent<Itako>().IsCutinEnabled = true;
    }

    private static void EnableJumpDrop()
    {
        ZunkoStatic.GetComponent<Zunko>().IsJumpDropEnabled = true;
        skillsStatic.SetCooldownVisible(SkillManager.Skill.Jump, true, !isLoading);
        if (!isLoading)
        {
            skillsStatic.ClearCooldownCount(SkillManager.Skill.Jump);
        }
        ZunkoStatic.GetComponent<Zunko>().BecomFun();
    }

    private static void PowerupZundaKuchiyose()
    {
        ItakoStatic.GetComponent<Itako>().IsPowerful = true;
        ItakoStatic.GetComponent<Itako>().BecomFun();
    }

    private static void PowerupZundaArrow()
    {
        ZunkoStatic.GetComponent<Zunko>().IsPowerful = true;
        ZunkoStatic.GetComponent<Zunko>().BecomFun();
    }

    private static void PowerupKiritanHou()
    {
        KiritanStatic.GetComponent<Kiritan>().IsPowerful = true;
        KiritanStatic.GetComponent<Kiritan>().BecomFun();
    }

    private static void AddEatingCutin()
    {
        ZunkoStatic.GetComponent<Zunko>().IsPositiveZunda = true;
    }

    private static void AddGraphButton()
    {
        if (!isLoading)
        {
            isGameCompleted = true;
        }
        else
        {
            GraphButtonStatic.SetActive(true);
        }
    }

    public static SkillManager skillsStatic;
    public static GameObject BackgroundStatic;
    public static GameObject AdditionalInformationStatic;
    public static GameObject ZunkoStatic;
    public static GameObject KiritanStatic;
    public static GameObject ItakoStatic;
    public static GameObject GraphButtonStatic;

    public SkillManager skills;
    public DataStorage storage;
    public GameObject Background;
    public GameObject AdditionalInformation;
    public GameObject Zunko;
    public GameObject Kiritan;
    public GameObject Itako;
    public GameObject GraphButton;

    public Button UnlockButton;
    public GameObject ExclamationIcon;
    public GameObject ZundaIcon;
    public Text AbstractMessage;
    public GameObject MessageBackground;


    public void RegisterCurrentZundaCount(int count)
    {
        lastRegisteredZundaCount = count;
        if (!UnlockButton.isActiveAndEnabled && (count >= NextUnlockZundaCount()))
        {
            // UnlockButton を押せる状態にする
            ShowAchievementMessage(true);
        }
    }

    public void UnlockAchievement()
    {
        UnlockAchievements(NextUnlockZundaCount());
    }

    public void SetFader(SceneFadeInOut fader)
    {
        sceneFader = fader;
    }

    void Start()
    {
        SetObjects();

        isLoading = true;
        ClearAchievements();
        UnlockAchievements(storage.UnlockedZundaCount);
        isLoading = false;
    }

    void Update()
    {
        if (isFadingShowBackground)
        {
            isFadingShowBackground = false;
            StartCoroutine(ShowBackgroundFading());
        }
        if (isGameCompleted)
        {
            isGameCompleted = false;
            CompleteGame();
        }
    }

    private void CompleteGame()
    {
        storage.Save();

        sceneFader.FadeColor = Color.white;
        sceneFader.FadeSpeed = 1.5f;
        sceneFader.EndScene("GameComplete");
    }

    private IEnumerator ShowBackgroundFading()
    {
        sceneFader.FadeSpeed = 0.25f;

        sceneFader.FadeColor = Color.white;
        sceneFader.StartFadeOut();
        while (sceneFader.IsFading())
        {
            yield return new WaitForSeconds(0.1f);
        }

        ChangeBackground();
        sceneFader.StartFadeIn();
    }

    private void SetObjects()
    {
        skillsStatic = skills;
        BackgroundStatic = Background;
        AdditionalInformationStatic = AdditionalInformation;
        ZunkoStatic = Zunko;
        KiritanStatic = Kiritan;
        ItakoStatic = Itako;
        GraphButtonStatic = GraphButton;
    }

    private void ClearAchievements()
    {
        Background.SetActive(false);
        AdditionalInformation.SetActive(false);
        Zunko.SetActive(false);
        Kiritan.SetActive(false);
        Itako.SetActive(false);
        GraphButton.SetActive(false);
    }

    private void UnlockAchievements(int unlockedZundaCount)
    {
        HideAchievementMessage();

        for (int i = unlockedIndex + 1; i < Events.Length; ++i)
        {
            Event e = Events[i];
            if (unlockedZundaCount >= e.UnlockZundaCount)
            {
                e.Action();
                storage.UnlockedZundaCount = unlockedZundaCount;
                unlockedIndex = i;
                if (!isLoading)
                {
                    GetComponent<AudioSource>().Play();
                }
                continue;
            }

            if (i > 0)
            {
                // １つでも実績が解除されていれば次の実績を表示する
                // （最初の実績は、解除できるようになるまで表示しない）
                bool isUnlockable = (lastRegisteredZundaCount >= NextUnlockZundaCount()) ? true : false;
                ShowAchievementMessage(isUnlockable);
            }

            break;
        }
    }

    private int NextUnlockZundaCount()
    {
        int nextIndex = unlockedIndex + 1;
        return (nextIndex >= Events.Length) ? int.MaxValue : Events[nextIndex].UnlockZundaCount;
    }

    private string NextUnlockMessage()
    {
        int nextIndex = unlockedIndex + 1;
        return (nextIndex == Events.Length) ? "" : Events[nextIndex].AbstractText;
    }

    private void ShowAchievementMessage(bool enabled)
    {
        UnlockButton.gameObject.SetActive(enabled);
        UnlockButton.enabled = enabled;
        ExclamationIcon.SetActive(enabled);

        ZundaIcon.SetActive(true);
        MessageBackground.SetActive(true);
        AbstractMessage.enabled = true;
        AbstractMessage.text = "x " + NextUnlockZundaCount().ToString() +  " ..." + NextUnlockMessage();
    }

    private void HideAchievementMessage()
    {
        UnlockButton.enabled = false;
        UnlockButton.gameObject.SetActive(false);
        ExclamationIcon.SetActive(false);
        ZundaIcon.SetActive(false);
        MessageBackground.SetActive(false);
        AbstractMessage.enabled = false;
    }
}
