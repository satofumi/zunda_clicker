﻿using UnityEngine;
using System.Collections;

public class SceneFadeInOut : MonoBehaviour
{
    private bool sceneStarting = true;      // Whether or not the scene is still fading in.
    private bool sceneEnding = false;
    private Texture2D texture;
    private float alpha = 1.0f;
    private float spentTime = 0;
    private string nextSceneName;

    public Color FadeColor
    {
        set;
        get;
    }

    public float FadeSpeed
    {
        set;
        get;
    }
    
    void Awake()
    {
        texture = new Texture2D(32, 32, TextureFormat.RGB24, false);
        texture.SetPixel(0, 0, Color.white);
        texture.Apply();
        nextSceneName = "";
        FadeColor = Color.black;
        FadeSpeed = 1.0f;
    }

    void Update()
    {
        if (sceneStarting)
        {
            FadeIn();
        }
        if (sceneEnding)
        {
            FadeOut();
        }
    }

    void OnGUI()
    {
        GUI.color = new Color(FadeColor.r, FadeColor.g, FadeColor.b, alpha);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);
    }
    
    void FadeIn()
    {
        alpha = Mathf.Lerp(1, 0, spentTime / FadeSpeed);
        spentTime += Time.deltaTime;

        if (alpha < 0.01)
        {
            alpha = 0;
            sceneStarting = false;
            spentTime = 0;
        }
    }

    void FadeOut()
    {
        alpha = Mathf.Lerp(0, 1, spentTime / FadeSpeed);
        spentTime += Time.deltaTime;

        if (alpha > 0.99f)
        {
            alpha = 1;
            sceneEnding = false;
            spentTime = 0;

            if (nextSceneName.Length > 0)
            {
                Application.LoadLevel(nextSceneName);
            }
        }
    }

    public void EndScene(string nextScene)
    {
        sceneEnding = true;
        sceneStarting = false;
        nextSceneName = nextScene;
        spentTime = alpha * FadeSpeed;
    }

    public bool IsFading()
    {
        return (sceneEnding || sceneStarting);
    }

    public void StartFadeOut()
    {
        EndScene("");
    }

    public void StartFadeIn()
    {
        sceneEnding = false;
        sceneStarting = true;
        spentTime = FadeSpeed - (alpha * FadeSpeed);
    }
}