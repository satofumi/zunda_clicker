﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Zunko : MonoBehaviour
{
    private enum View : int
    {
        Stand = 0,
        Eating = 1,
        Happy = 2,
        Sleeping = 3,
        Zunda = 4,
        Arrow = 5,
        Bow = 6,
        ArrowRight = 7,
    }

    private enum State : int
    {
        Stand,
        Move,
        Sleeping,
        Surprised,
        Eat,
        ZundaArrow,
        JumpDrop,
    }

    private static readonly int orderOffset = 3;
    private static readonly float JumpInterval = 2.0f;

    private Animator animator;
    private Renderer characterRenderer;
    private State state;
    private bool isProcessing;
    Vector3? groundedZunda;
    private float previousEatCutinTiming;
    private AudioSource audioSource;

    public GameController gameController;
    public SkillManager skills;
    public ZundaStorage zunda;

    public GameObject JumpingZunko;
    public GameObject KinakoMochi;
    public GameObject NoneMochi;
    public GameObject AnMochi;
    public GameObject ZundaWarp;
    public GameObject ZundaArrow;
    public GameObject SmallBomb;

    public GameObject CutinJumpDrop;
    public GameObject CutinEatZunda;

    public GameObject CutinZundaArrow;

    public AudioClip Hello;
    public AudioClip Happy;
    public AudioClip SleepStart;
    public AudioClip EatStart;
    public AudioClip JumpSkillStart;
    public AudioClip Jumping1;
    public AudioClip Jumping2;
    public AudioClip ArrowStart;
    public AudioClip Tadaima;
    public AudioClip Okaeri1;
    public AudioClip Okaeri2;
    public AudioClip JumpEffect;

    public bool IsEatingEnabled
    {
        set;
        get;
    }

    public bool IsZundaArrowEnabled
    {
        set;
        get;
    }

    public bool IsJumpDropEnabled
    {
        set;
        get;
    }

    public bool IsCutinEnabled
    {
        set;
        get;
    }

    public bool IsPowerful
    {
        set;
        get;
    }

    public bool IsPositiveZunda
    {
        set;
        get;
    }

    public void ResolveClogged()
    {
        if (state != State.Stand)
        {
            return;
        }

        // ずん子はずんだ餅を食べて寝ていることが多いので、スキルを発動しやすい確率にする
        if (Random.value < 0.25f)
        {
            return;
        }

        if (!skills.LockSkillActivation())
        {
            return;
        }
        // ここに到達した時点で Skill リソースをロックしている

        // スキルを発動させる
        bool isArrowEnabled = IsZundaArrowEnabled && !skills.IsCooldown(SkillManager.Skill.Arrow);
        bool isJumpEnabled = IsJumpDropEnabled && !skills.IsCooldown(SkillManager.Skill.Jump);
        if (isArrowEnabled && isJumpEnabled)
        {
            // どちらかのスキルを発動させる
            state = (Random.value > 0.5f) ? State.ZundaArrow : State.JumpDrop;
        }
        else
        {
            if (isArrowEnabled)
            {
                // ずんだアローを発動させる
                state = State.ZundaArrow;
            }
            else if (isJumpEnabled)
            {
                // もちもちジャンプを発動させる
                state = State.JumpDrop;
            }
            else
            {
                // 発動できるスキルがなかったのでロックを解除する
                skills.UnlockSkillActivation();
            }
        }
    }

    public void BecomFun()
    {
        if (state == State.Stand)
        {
            state = State.Surprised;
        }
    }

    void OnDisable()
    {
        skills.SetCooldownVisible(SkillManager.Skill.Jump, false);
        skills.SetCooldownVisible(SkillManager.Skill.Arrow, false);
    }

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        animator = GetComponent<Animator>();
        characterRenderer = GetComponent<Renderer>();
        state = State.Move;
    }

    void Update()
    {
        UpdateState();
    }

    private void UpdateState()
    {
        if (isProcessing)
        {
            return;
        }

        isProcessing = true;
        switch (state)
        {
            case State.Stand:
                StartCoroutine(ProcessStand());
                break;

            case State.Move:
                StartCoroutine(ProcessMove());
                break;

            case State.Sleeping:
                StartCoroutine(ProcessSleeping());
                break;

            case State.Surprised:
                StartCoroutine(ProcessSurprised());
                break;

            case State.Eat:
                StartCoroutine(ProcessEat());
                break;

            case State.ZundaArrow:
                StartCoroutine(ProcessZundaArrow());
                break;

            case State.JumpDrop:
                StartCoroutine(ProcessJumpDrop());
                break;
        }
    }

    private void ChangeView(View value)
    {
        animator.SetInteger("state", (int)value);
    }

    private void ChangeOrder(int order)
    {
        characterRenderer.sortingOrder = orderOffset + order;
    }

    private void ChangeToNextState()
    {
        if (state != State.Stand)
        {
            // コルーチン処理中に ResolveClogged() でスキル使用により state が変更されている場合には戻る
            return;
        }

        // 他の State へランダムに遷移させる
        float randomValue = Random.value;
        if (randomValue > 0.98)
        {
            state = State.Move;
        }

        if (state == State.Stand)
        {
            if (IsEatingEnabled)
            {
                groundedZunda = zunda.GroundedZundaPosition();
                if (groundedZunda.HasValue)
                {
                    if (skills.LockSkillActivation())
                    {
                        state = State.Eat;
                    }
                }
            }
        }
    }

    private IEnumerator ProcessStand()
    {
        ChangeView(View.Stand);
        ChangeOrder(Character.Middle);

        Vector3 upDownWidth = new Vector3(0, 0.0175f, 0);
        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, 0.225f, 1));

        ChangeToNextState();
        isProcessing = false;
    }

    private IEnumerator ProcessMove()
    {
        ChangeView(View.Stand);
        ChangeOrder(Character.Back);

        Vector3 upDownWidth = new Vector3(0, 0.0175f, 0);
        yield return StartCoroutine(Character.MoveTo(transform, Character.RandomPositionX(), 0.05f, upDownWidth));

        state = State.Stand;
        isProcessing = false;
    }

    private IEnumerator ProcessSleeping()
    {
        audioSource.clip = SleepStart;
        audioSource.Play();

        ChangeView(View.Sleeping);
        ChangeOrder(Character.Front);

        yield return StartCoroutine(Character.Sleep(this, transform));

        state = State.Stand;
        isProcessing = false;
    }

    private IEnumerator ProcessSurprised()
    {
        audioSource.clip = Happy;
        audioSource.Play();

        ChangeView(View.Happy);
        ChangeOrder(Character.Front);

        Vector3 upDownWidth = new Vector3(0, 0.03f, 0);
        float intervalSecond = 0.3f;
        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, intervalSecond, (int)(Character.SurprisedSeconds / intervalSecond)));

        state = State.Stand;
        isProcessing = false;
    }

    // ずんだ餅を食べる
    private IEnumerator ProcessEat()
    {
        ChangeView(View.Stand);
        ChangeOrder(Character.Middle);

        // ずんだ餅のある位置まで移動させる
        Vector3 upDownWidth = new Vector3(0, 0.0175f, 0);
        yield return StartCoroutine(Character.MoveTo(transform, groundedZunda.Value.x, 0.05f, upDownWidth));

        zunda.RemoveFirstZunda();

        // ずんだ餅を持って喜ぶ
        ChangeView(View.Zunda);
        ChangeOrder(Character.Front);
        yield return new WaitForSeconds(2.0f);

        // カットインを表示する（カットインは連続で表示されないようにする）
        float EatCutinIntervalSeconds = 16.0f;
        float now = Time.time;
        if ((now - previousEatCutinTiming) > EatCutinIntervalSeconds)
        {
            if (IsCutinEnabled)
            {
                CutinEatZunda.GetComponent<Cutin>().Label = IsPositiveZunda ? " \"EAT Meee!!!\"" : "「おいしいよ！」";
                yield return StartCoroutine(Character.PlayCutin(this, CutinEatZunda));
            }
        }
        previousEatCutinTiming = now;

        audioSource.clip = EatStart;
        audioSource.Play();

        // ずんだ餅を食べだしたときには、他のスキルが発動できるようにする
        skills.UnlockSkillActivation();
        ChangeView(View.Eating);
        yield return new WaitForSeconds(3.0f);

        zunda.IncrementZundaCount();

        if (zunda.GroundedZundaPosition().HasValue)
        {
            // 次のずんだ餅がある場合は、Sleep しない
            state = State.Stand;
        }
        else
        {
            state = State.Sleeping;
        }
        isProcessing = false;
    }

    // 「ずんだアロー」
    private IEnumerator ProcessZundaArrow()
    {
        audioSource.clip = ArrowStart;
        audioSource.Play();

        ChangeView(View.Bow);
        ChangeOrder(Character.Front);

        if (IsCutinEnabled)
        {
            yield return StartCoroutine(Character.PlayCutin(this, CutinZundaArrow));
        }

        // 空中に餅を召還して、ずんだアローで射る
        yield return StartCoroutine(ShootZundaArrows());
        yield return new WaitForSeconds(2.0f);

        skills.UnlockSkillActivation();
        skills.ResetCooldownCount(SkillManager.Skill.Arrow);
        state = State.Stand;
        isProcessing = false;
    }

    private IEnumerator ShootZundaArrows()
    {
        ChangeView(View.Bow);
        Vector3 upDownWidth = new Vector3(0, 0.15f, 0);
        yield return StartCoroutine(Character.ShakeLoop(transform, upDownWidth, 0.15f, 2));
        yield return new WaitForSeconds(0.5f);

        ChangeView(View.Arrow);

        // 餅を中空に配置する
        float shootInterval = 0.2f;
        var mochis = new List<GameObject>();

        // ずんだアローの本数
        int arrowsCount = IsPowerful ? 20 : 1;
        for (int i = 0; i < arrowsCount; ++i)
        {
            // 餅の配置
            float x = Random.Range(Movable.MochiArea.x, Movable.MochiArea.x + Movable.MochiArea.width);
            float y = Random.Range(Movable.MochiArea.y - Movable.MochiArea.height, Movable.MochiArea.y);
            Vector3 position = new Vector3(x, y, 0);
            int typeValue = Random.Range(0, 3);
            GameObject type = (typeValue == 0) ? NoneMochi : (typeValue == 1) ? AnMochi : KinakoMochi;
            GameObject mochi = (GameObject)Instantiate(type, position, Quaternion.identity);
            mochis.Add(mochi);

            // エフェクト
            GameObject warp = (GameObject)Instantiate(ZundaWarp, position, Quaternion.identity);
            Destroy(warp, 0.45f);

            // 上昇する矢の配置
            GameObject zundaArrow = (GameObject)Instantiate(ZundaArrow, new Vector3(transform.position.x + Random.Range(-0.0f, 0.4f), -1.0f, 0), Quaternion.identity);
            zundaArrow.transform.Rotate(new Vector3(0, 0, 90.0f));
            Rigidbody2D arrowBody = zundaArrow.GetComponent<Rigidbody2D>();
            arrowBody.velocity = new Vector2(0, 20.0f);
            Destroy(zundaArrow, 2.0f);

            yield return new WaitForSeconds(shootInterval);
        }
        
        ChangeView(View.ArrowRight);

        // 落下効果音の再生開始
        yield return new WaitForSeconds(0.75f);
        mochis[0].GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(1.00f);

        // 矢の生成処理と命中処理
        List<GameObject> arrows = new List<GameObject>();
        float spentTime = 0.0f;
        int arrowCreatedCount = 0;
        int leftCount = mochis.Count;
        while (leftCount > 0)
        {
            spentTime += Time.deltaTime;

            if ((spentTime > (shootInterval * arrowCreatedCount)) && (arrowCreatedCount < mochis.Count))
            {
                GameObject zundaArrow = (GameObject)Instantiate(ZundaArrow, new Vector3(mochis[arrowCreatedCount].transform.position.x, 6, 0), Quaternion.identity);
                zundaArrow.transform.Rotate(new Vector3(0, 0, -90.0f));
                Rigidbody2D arrowBody = zundaArrow.GetComponent<Rigidbody2D>();
                arrowBody.velocity = new Vector2(0, -20.0f);
                arrows.Add(zundaArrow);

                ++arrowCreatedCount;
            }


            for (int i = 0; i < arrows.Count; ++i)
            {
                if (arrows[i] == null)
                {
                    continue;
                }
                GameObject mochi = mochis[i];
                GameObject arrow = arrows[i];
                float mochiY = mochi.transform.position.y;
                float arrowY = arrow.transform.position.y;
                if (arrowY < mochiY)
                {
                    // 高速落下する Zunda を配置する
                    zunda.CreateZunda(mochi.transform.position, new Vector2(0, -50.0f));

                    // エフェクトの配置
                    GameObject bomb = (GameObject)Instantiate(SmallBomb, mochi.transform.position, Quaternion.identity);
                    Destroy(bomb, 0.2f);
                    Rigidbody2D bombBody = bomb.GetComponent<Rigidbody2D>();
                    bombBody.velocity = new Vector2(0, -2.0f);

                    bomb.GetComponent<AudioSource>().Play();

                    // ずんだアローと餅の削除
                    Destroy(arrow);
                    Destroy(mochi);
                    arrows[i] = null;
                    --leftCount;
                }
            }

            yield return new WaitForSeconds(0.01f);
        }

        yield return new WaitForSeconds(0.5f);
        ChangeView(View.Bow);
    }

    // 「もちもちジャンプ」
    private IEnumerator ProcessJumpDrop()
    {
        audioSource.clip = JumpSkillStart;
        audioSource.Play();

        ChangeView(View.Happy);
        ChangeOrder(Character.Front);

        if (IsCutinEnabled)
        {
            yield return StartCoroutine(Character.PlayCutin(this, CutinJumpDrop));
        }

        // 移動用のずん子を画面外に移動させ、ジャンプ用のずん子を配置する
        audioSource.clip = JumpEffect;
        audioSource.Play();

        Vector3 jumpVelocity = new Vector3(0, 0.4f, 0);
        yield return StartCoroutine(Character.ShakeLoop(transform, jumpVelocity, 0.015f, 20, true));
        transform.position = Movable.ScreenoutLeft;
        yield return new WaitForSeconds(0.75f);
        yield return StartCoroutine(ShowJumpingZunko());

        // デスクトップマスコットを表示し、プロセスが終了するまで待つ
        yield return StartCoroutine(ExecDesktopZunko());
        yield return new WaitForSeconds(1.0f);

        skills.UnlockSkillActivation();
        skills.ResetCooldownCount(SkillManager.Skill.Jump);
        state = State.Move;
        isProcessing = false;

        yield return StartCoroutine(PlayGreetings());
    }

    private IEnumerator ShowJumpingZunko()
    {
        yield return new WaitForSeconds(1.0f);
        GameObject zunko = (GameObject)Instantiate(JumpingZunko, new Vector3(1, 5, 0), Quaternion.identity);
        zunko.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -8.0f);
        Rigidbody2D body = zunko.GetComponent<Rigidbody2D>();

        float pollingInterval = 0.2f;
        float interval = 0;
        float direction = 1.0f;
        while (true)
        {
            yield return new WaitForSeconds(pollingInterval);

            // 穴に落ちたら取り除く
            if (zunko.transform.position.y < Movable.DroppedY)
            {
                break;
            }

            // 一定時間ごとにジャンプさせる
            interval += pollingInterval;
            if ((interval > JumpInterval) && (body.velocity.magnitude < 0.01))
            {
                audioSource.clip = (Random.value > 0.5f) ? Jumping1 : Jumping2;
                audioSource.Play();

                interval -= JumpInterval;
                direction = -direction;
                body.velocity = new Vector2(direction, 20.0f);
                gameController.SendMessage("ResolveCloggedByCharacter");
                zunko.GetComponent<AudioSource>().Play();
            }

        }
        Destroy(zunko);
    }

    private IEnumerator PlayGreetings()
    {
        yield return new WaitForSeconds(2.0f);

        audioSource.clip = Tadaima;
        audioSource.Play();

        yield return new WaitForSeconds(2.0f);

        audioSource.clip = (Random.value > 0.5f) ? Okaeri1 : Okaeri2;
        audioSource.Play();
    }

    private IEnumerator ExecDesktopZunko()
    {
        if (!Screen.fullScreen)
        {
            WalkingZunko.Execute();

            while (WalkingZunko.IsWalking())
            {
                yield return new WaitForSeconds(0.5f);
            }
        }
    }

    public void PlayHello()
    {
        audioSource.clip = Hello;
        audioSource.Play();
    }
}
