﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DonateController : MonoBehaviour
{
    private static int PreviousTotalCount = 0;

    public string BaseUrl = "http://www.hyakuren-soft.sakura.ne.jp/ZundaClicker/";
    public DataStorage storage;
    public Button DonateButton;
    public Button BackButton;
    public Text TotalZundaCount;
    public Text CurrentZundaCount;
    public GameObject ZundaPrefab;

    private int totalCount;
    private bool isSuccess;
    private int previousZundaCount;

    public void DonateZunda()
    {
        if (storage.ZundaCount <= 0)
        {
            return;
        }

        DonateButton.enabled = false;
        StartCoroutine(ProcessDonateZunda());
    }

    private IEnumerator ProcessDonateZunda()
    {
        // 登録
        string key = storage.RegisterKey;
        int count = storage.ZundaCount;

        // 処理が途中で中断しないように、BACK ボタンを無効にする
        BackButton.enabled = false;

        // 送信処理
        isSuccess = false;
        yield return StartCoroutine(SendZundaCount(key, count));
        if (!isSuccess)
        {
            SetNoNetworkMessages();
            yield break;
        }

        // 現在のカウントを減らしつつ、登録した数だけずんだ餅を降らせるようにする
        StartCoroutine(dropZunda(count));

        // ローカルデータのリセット
        storage.ResetData();
        storage.Save();

        // Back ボタンを有効にする
        BackButton.enabled = true;

        // 最新データの取得
        previousZundaCount = count;
        yield return StartCoroutine(UpdateZundaInformation(false));

        DonateButton.enabled = true;
    }

    public void ChangeTo(string sceneName)
    {
        GetComponent<SceneFadeInOut>().EndScene(sceneName);
    }

    void Start()
    {
        // Donate ボタンを無効にしてから処理を開始し、正常処理後に有効にする
        DonateButton.enabled = false;
        previousZundaCount = storage.ZundaCount;
        StartCoroutine(UpdateZundaInformation(true));
    }

    private void SetTotalCount(int count)
    {
        TotalZundaCount.enabled = true;
        TotalZundaCount.text = count.ToString("#,0");
    }

    private void SetZundaCount(int count)
    {
        CurrentZundaCount.text = count.ToString("#,0");
    }

    private IEnumerator dropZunda(int count)
    {
        for (int i = 0; i < (count / 10); ++i)
        {
            for (int k = 0; k < 10; ++k)
            {
                float x = Random.Range(-0.1f, +0.1f);
                GameObject zunda = (GameObject)Instantiate(ZundaPrefab, new Vector3(x, 6, 0), Quaternion.identity);
                Destroy(zunda, 5);
            }
            yield return new WaitForSeconds(0.01f);
        }
    }

    private IEnumerator UpdateZundaInformation(bool immediate)
    {
        // 登録キーの取得
        if (string.IsNullOrEmpty(storage.RegisterKey))
        {
            yield return StartCoroutine(RequestRegisterKey());
        }
        string key = storage.RegisterKey;
        if (string.IsNullOrEmpty(key))
        {
            SetNoNetworkMessages();
            yield break;
        }

        // 情報の取得と更新
        isSuccess = false;
        yield return StartCoroutine(RequestZundaInformation(key));
        if (!isSuccess)
        {
            SetNoNetworkMessages();
            yield break;
        }

        StartCoroutine(CountUpTotalCount(totalCount, storage.ZundaCount, immediate));

        // Donate ボタンを押せるようにする
        DonateButton.enabled = true;
    }

    private IEnumerator RequestRegisterKey()
    {
        string url = BaseUrl + "request_key.php";
        WWW www = new WWW(url);
        yield return www;
        if (string.IsNullOrEmpty(www.error))
        {
            isSuccess = true;
            storage.RegisterKey = www.text;
            storage.Save();
        }
    }

    private void SetNoNetworkMessages()
    {
        // !!!
    }

    private IEnumerator RequestZundaInformation(string key)
    {
        string url = BaseUrl + "information.php" + "?key=" + System.Uri.EscapeDataString(key);
        WWW www = new WWW(url);
        yield return www;
        if (string.IsNullOrEmpty(www.error))
        {
            isSuccess = true;
            Debug.Log(www.text);
            totalCount = int.Parse(www.text);
        }
    }

    private IEnumerator SendZundaCount(string key, int count)
    {
        string url = BaseUrl + "donate.php" + "?key=" + System.Uri.EscapeDataString(key) + "&zunda=" + count.ToString();
        WWW www = new WWW(url);
        yield return www;
        if (string.IsNullOrEmpty(www.error))
        {
            isSuccess = true;
        }
    }

    private IEnumerator CountUpTotalCount(int nextTotalCount, int nextCurrentCount, bool immediate)
    {
        if (!immediate)
        {
            AudioSource countDownSound = GetComponent<AudioSource>();
            countDownSound.Play();

            int divisor = 400;
            for (int i = 0; i < divisor; ++i)
            {
                int total = PreviousTotalCount + (i * (nextTotalCount - PreviousTotalCount) / divisor);
                SetTotalCount(total);

                int current = previousZundaCount + (i * (nextCurrentCount - previousZundaCount) / divisor);
                SetZundaCount(current);
                
                yield return new WaitForSeconds(0.02f);
            }
            countDownSound.Stop();
        }
        PreviousTotalCount = nextTotalCount;
        SetTotalCount(nextTotalCount);
        SetZundaCount(nextCurrentCount);
    }
}
