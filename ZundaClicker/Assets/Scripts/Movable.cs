﻿using UnityEngine;
using System.Collections;

public class Movable
{
    // 穴より左側の移動範囲
    public static readonly float LeftMin = -6.1f;
    public static readonly float LeftMax = -1.7f;

    // 穴より右側の移動範囲
    public static readonly float RightMin = 1.7f;
    public static readonly float RightMax = 2.8f;

    // 地面の高さ
    public static readonly float GroundY = -4.7f;

    // 落下したと判断する Y の値
    public static readonly float DroppedY = -8.0f;

    // 画面からはみ出たと判断する値
    public static readonly float WindowLeft = -7.0f;
    public static readonly float WindowRight = 7.0f;
    public static readonly float WindowTop = 5.0f;
    public static readonly float WindowBottom = -5.0f;

    // 左側の画面外の位置
    public static readonly Vector3 ScreenoutLeft = new Vector3(-8.0f, GroundY, 0);

    // 餅が地面に落ちてると判断する領域
    public static readonly float GroundRightEdge = -1.0f;
    public static readonly float GroundTopEdge = -2.0f;

    // きりたん砲を発射する位置
    public static readonly float KiritanFirePosition = -0.8f;

    // ずんだアロー前に召還する餅の出現範囲
    public static Rect MochiArea = new Rect(-1.5f, 1.5f, 3.0f, 2.0f);
}
