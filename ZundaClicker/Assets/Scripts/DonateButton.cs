﻿using UnityEngine;
using System.Collections;

public class DonateButton : MonoBehaviour
{
    public DonateController controller;

    public void DonateButtonPressed()
    {
        controller.DonateZunda();
        GetComponent<AudioSource>().Play();
    }
}
