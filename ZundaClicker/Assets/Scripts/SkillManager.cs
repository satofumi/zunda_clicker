﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class SkillManager : MonoBehaviour
{
    public enum Skill : int
    {
        Jump = 0,
        Arrow = 1,
        Itako = 2,
        Kiritan = 3,
    }

    private static Skill[] AllSkill;
    private static readonly float MaxCooldownCount = 90.0f;
    
    private bool IsSkillActivating = false;

    private float secondsInterval;
    private Vector3 previousMousePosition;

    private class Cooldown
    {
        public float leftSeconds;

        public Cooldown()
        {
            leftSeconds = MaxCooldownCount;
        }
    }

    static private Dictionary<Skill, Cooldown> skills;
    private Text[] CooldownTexts;
    private GameObject[] IconPrefabs;
    private Animator[] IconAnimators;
    private Dictionary<Collider2D, string> skillDescriptions;
    private Vector3 skillDescriptionFirstPosition;
    private Vector3 skillBackgroundFirstPosition;

    public Text JumpCooldownText;
    public Text ArrowCooldownText;
    public Text ItakoCooldownText;
    public Text KiritanCooldownText;
    public Text SkillDescriptionText;
    public GameObject SkillDescriptionBackground;

    public GameObject JumpIcon;
    public GameObject ArrowIcon;
    public GameObject ItakoIcon;
    public GameObject KiritanIcon;

    public GameObject ShineEffect;

    void Awake()
    {
        if (AllSkill == null)
        {
            AllSkill = new Skill[] { Skill.Jump, Skill.Arrow, Skill.Itako, Skill.Kiritan };
        }
        if (skills == null)
        {
            skills = new Dictionary<Skill, Cooldown>();
            
            foreach (var skill in AllSkill)
            {
                skills[skill] = new Cooldown();
            }
            InitializeSkills();
        }

        skillDescriptions = new Dictionary<Collider2D, string>();
        InitializeSkillDescriptions();

        CooldownTexts = new Text[AllSkill.Length];
        CooldownTexts[(int)Skill.Jump] = JumpCooldownText;
        CooldownTexts[(int)Skill.Arrow] = ArrowCooldownText;
        CooldownTexts[(int)Skill.Itako] = ItakoCooldownText;
        CooldownTexts[(int)Skill.Kiritan] = KiritanCooldownText;

        IconPrefabs = new GameObject[AllSkill.Length];
        IconPrefabs[(int)Skill.Jump] = JumpIcon;
        IconPrefabs[(int)Skill.Arrow] = ArrowIcon;
        IconPrefabs[(int)Skill.Itako] = ItakoIcon;
        IconPrefabs[(int)Skill.Kiritan] = KiritanIcon;

        IconAnimators = new Animator[AllSkill.Length];
        IconAnimators[(int)Skill.Jump] = JumpIcon.GetComponent<Animator>();
        IconAnimators[(int)Skill.Arrow] = ArrowIcon.GetComponent<Animator>();
        IconAnimators[(int)Skill.Itako] = ItakoIcon.GetComponent<Animator>();
        IconAnimators[(int)Skill.Kiritan] = KiritanIcon.GetComponent<Animator>();
    }

    void Start()
    {
        skillDescriptionFirstPosition = SkillDescriptionText.transform.position;
        skillBackgroundFirstPosition = SkillDescriptionBackground.transform.position;
    }

    void Update()
    {
        CountDown();
        ShowSkillDescription();
    }

    private void InitializeSkills()
    {
        foreach (var skill in AllSkill)
        {
            skills[skill].leftSeconds = 0;
        }
    }

    private void InitializeSkillDescriptions()
    {
        skillDescriptions[JumpIcon.GetComponent<Collider2D>()] =
            "<b>もちもちジャンプ <color=#ff9999><size=14>（zps が 0 のときに発動）</size></color></b>\n" +
            "ずんだ餅に囲まれる至福のずんだタイム。\nと〜っても、もちもちである。";

        skillDescriptions[ArrowIcon.GetComponent<Collider2D>()] =
            "<b>ずんだアロー <color=#ff9999><size=14>（zps が 0 のときに発動）</size></color></b>\n" + 
            "餅を召還し、矢を命中させてずんだ餅に変える\n一発必中のずんだ技。もちもちである。";
        
        skillDescriptions[ItakoIcon.GetComponent<Collider2D>()] =
            "<b>ずんだ口寄せ <color=#ff9999><size=14>（zps が 0 のときに発動）</size></color></b>\n" +
            "ずんだ餅を霊界に送り込み、再び召還して穴に\n落とす ZUNDA 技。凄さが理解されにくい。";
        
        skillDescriptions[KiritanIcon.GetComponent<Collider2D>()] =
            "<b>きりたん砲 <color=#ff9999><size=14>（zps が 0 のときに発動）</size></color></b>\n" +
            "背中の砲門から味噌を打ち出し、詰まりも\n何もかもを吹き飛ばす便利な範囲攻撃。";
    }

    private void CountDown()
    {
        const float updateInterval = 0.1f;
        secondsInterval += Time.deltaTime;
        if (secondsInterval > updateInterval)
        {
            secondsInterval -= updateInterval;
            foreach (var skill in AllSkill)
            {
                Cooldown cooldown = skills[skill];
                cooldown.leftSeconds = Mathf.Max(0, cooldown.leftSeconds - updateInterval);
                UpdateCooldown(skill, cooldown.leftSeconds);
            }
        }
    }

    private void UpdateCooldown(Skill skill, float count)
    {
        if (!IconPrefabs[(int)skill].activeSelf)
        {
            return;
        }

        string text;
        bool enable;
        if (count > 0)
        {
            text = (count < 1.0f) ? count.ToString("F1") : count.ToString("F0");
            enable = false;
        }
        else
        {
            text = "";
            enable = true;
        }

        CooldownTexts[(int)skill].text = text;
        IconAnimators[(int)skill].SetBool("enable", enable);
    }
    
    private void ShowSkillDescription()
    {
        if (Input.mousePosition == previousMousePosition)
        {
            return;
        }
        previousMousePosition = Input.mousePosition;

        bool isFocused = FocusedSkillDescription();
        SkillDescriptionText.enabled = isFocused;
        SkillDescriptionBackground.SetActive(isFocused);
    }

    private bool FocusedSkillDescription()
    {
        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Collider2D hit = Physics2D.OverlapPoint(worldPoint);
        if (hit == null)
        {
            return false;
        }
        if (!skillDescriptions.ContainsKey(hit))
        {
            return false;
        }

        // 説明の表示
        string description = skillDescriptions[hit];
        SkillDescriptionText.text = description;

        Vector3 basePoint = JumpIcon.GetComponent<Collider2D>().transform.position;
        SkillDescriptionText.transform.position = skillDescriptionFirstPosition + (hit.transform.position - basePoint);
        SkillDescriptionBackground.transform.position = skillBackgroundFirstPosition + (hit.transform.position - basePoint);

        return true;
    }


    public void DecrementCooldowns()
    {
        foreach (var skill in AllSkill)
        {
            Cooldown cooldown = skills[skill];
            --cooldown.leftSeconds;
        }
        secondsInterval = 0;
    }

    public void SetCooldownVisible(Skill skill, bool enable, bool drawEffect = false)
    {
        GameObject icon = IconPrefabs[(int)skill];
        if (icon != null)
        {
            icon.SetActive(enable);
            if (enable)
            {
                Vector3 offset = new Vector3(0, -0.5f, 0);
                if (drawEffect)
                {
                    GameObject shine = (GameObject)Instantiate(ShineEffect, icon.transform.position - offset, Quaternion.identity);
                    Destroy(shine, 1.0f);
                }
            }
        }

        // Update() で更新されるので、enable に関係なく表示を消す
        Text text = CooldownTexts[(int)skill];
        if (text != null)
        {
            CooldownTexts[(int)skill].text = "";
        }
    }

    public bool LockSkillActivation()
    {
        if (IsSkillActivating)
        {
            return false;
        }

        IsSkillActivating = true;
        return true;
    }

    public void UnlockSkillActivation()
    {
        IsSkillActivating = false;
    }

    public void ResetCooldownCount(Skill skill)
    {
        Cooldown cooldown = skills[skill];
        cooldown.leftSeconds = MaxCooldownCount;
    }

    public void ClearCooldownCount(Skill skill)
    {
        Cooldown cooldown = skills[skill];
        cooldown.leftSeconds = 0;
    }

    public bool IsCooldown(Skill skill)
    {
        Cooldown cooldown = skills[skill];
        return (cooldown.leftSeconds > 0) ? true : false;
    }
}
