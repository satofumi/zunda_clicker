﻿using UnityEngine;
using System.Collections;

public class UnlockButton : MonoBehaviour
{
    public AchievementManager achievements;

    public void UnlockButtonPressed()
    {
        achievements.UnlockAchievement();
    }
}
