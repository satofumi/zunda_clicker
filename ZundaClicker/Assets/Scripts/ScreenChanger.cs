﻿using UnityEngine;
using System.Collections;
using System.Text;

public class ScreenChanger : MonoBehaviour
{
    void Start()
    {
        StartCoroutine(ChangeResolution());
    }

    private IEnumerator ChangeResolution()
    {
#if UNITY_EDITOR
        yield break;
#elif UNITY_STANDALONE_WIN
        const int width = 800;
        const int height = 600;
        if ((Screen.width != width) || (Screen.height != height))
        {
            Screen.SetResolution(width, height, false);
            yield return new WaitForEndOfFrame();
            Reboot();
        }
#endif
    }

#if UNITY_STANDALONE_WIN
    private void Reboot()
    {
        // 実行パス名を取得する
        string[] s = System.Reflection.Assembly.GetExecutingAssembly().Location.Split('\\');
        StringBuilder location = new StringBuilder();
        for (int i = 0; i < s.Length - 3; i++)
        {
            location.Append(s[i] + '\\');
        }
        location.Append("ZundaClicker.exe");
        string exe = location.ToString();

        // 再起動する
        System.Diagnostics.ProcessStartInfo start = new System.Diagnostics.ProcessStartInfo();
        start.FileName = exe;
        System.Diagnostics.Process.Start(start);

        Application.Quit();
    }
#endif
}
