﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Cutin : MonoBehaviour
{
    private static Text LabelText;
    private static Vector3 FirstPosition;

    public string Label;

    public void SetCutinLabel(Text text)
    {
        LabelText = text;
        LabelText.enabled = false;
        FirstPosition = text.transform.position;
    }

    public IEnumerator Play()
    {
        LabelText.text = Label;
        LabelText.transform.position = FirstPosition;
        LabelText.enabled = true;
        Transform label = LabelText.transform;
        Transform character = transform.Find("Character");

        Vector3 characterMoveWidth = new Vector3(-0.5f, 0, 0);
        Vector3 labelMoveWidth = new Vector3(20f, 0, 0);

        int loopTimes = 80;
        float seconds = 1.30f;
        for (int i = 0; i < loopTimes; ++i)
        {
            character.position += characterMoveWidth / loopTimes;
            label.position += labelMoveWidth / loopTimes;

            yield return new WaitForSeconds(seconds / loopTimes);
        }

        LabelText.enabled = false;
    }
}
