﻿using UnityEngine;
using System.Collections;

public class GraphButton : MonoBehaviour
{
    public GameController controller;

    public void GraphButtonPressed()
    {
        controller.ChangeTo("Donate");
    }
}
