﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.IO;

public class FallingZunda
{
    static public void Execute(float vx, float vy, float x, float y)
    {
        var startInfo = new ProcessStartInfo();
        startInfo.FileName = Directory.GetCurrentDirectory() + "\\ZundaClicker_Data\\FallingZunda.exe";
        startInfo.CreateNoWindow = true;
        startInfo.UseShellExecute = false;
        int screenX = (int)(x * 60.0f);
        int screenY = (int)(-y * 60.0f);
        startInfo.Arguments = vx.ToString() + "x" + (-vy).ToString() + "+" + screenX.ToString() + "+" + screenY.ToString();

        try
        {
            Process.Start(startInfo);
        }
        catch (System.Exception)
        {
            // 何もしない
        }
    }
}
