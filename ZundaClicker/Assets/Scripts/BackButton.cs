﻿using UnityEngine;
using System.Collections;

public class BackButton : MonoBehaviour
{
    public DonateController controller;

    public void BackButtonPressed()
    {
        controller.ChangeTo("Clicker");
    }
}
