﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.IO;

public class WalkingZunko
{
    static private Process process = null;

    // WalkingZunko を起動する
    static public void Execute()
    {
        var startInfo = new ProcessStartInfo();
        startInfo.FileName = Directory.GetCurrentDirectory() + "\\ZundaClicker_Data\\WalkingZunko.exe";
        startInfo.CreateNoWindow = true;
        startInfo.UseShellExecute = false;

        try
        {
            process = Process.Start(startInfo);
        }
        catch (System.Exception ex)
        {
            // 何もしない
            System.Diagnostics.Debug.WriteLine(ex.ToString());
        }
    }

    // WalkingZunko が動作中かを返す
    static public bool IsWalking()
    {
        if (process == null)
        {
            return false;
        }

        if (process.HasExited)
        {
            process = null;
            return false;
        }
        else
        {
            return true;
        }
    }
}
