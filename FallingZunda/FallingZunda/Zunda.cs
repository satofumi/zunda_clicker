﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace FallingZunda
{
    public partial class Zunda : Form
    {
        const string ZundaImage = "FallingZunda.zzm_zunda_small.png";

        private PointF position;
        private PointF velocity;

        [StructLayout(LayoutKind.Sequential)]
        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }
        
        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int DeleteObject(IntPtr hobject);

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private struct BLENDFUNCTION
        {
            public byte BlendOp;
            public byte BlendFlags;
            public byte SourceConstantAlpha;
            public byte AlphaFormat;
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int UpdateLayeredWindow(
            IntPtr hwnd,
            IntPtr hdcDst,
            [System.Runtime.InteropServices.In()]
            ref Point pptDst,
            [System.Runtime.InteropServices.In()]
            ref Size psize,
            IntPtr hdcSrc,
            [System.Runtime.InteropServices.In()]
            ref Point pptSrc,
            int crKey,
            [System.Runtime.InteropServices.In()]
            ref BLENDFUNCTION pblend,
            int dwFlags);

        [DllImport("user32")]
        private static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern int GetWindowRect(IntPtr hwnd, ref RECT lpRect);

        protected override CreateParams CreateParams
        {
            get
            {
                const int WS_EX_LAYERED = 0x00080000;
                CreateParams cp = base.CreateParams;
                cp.ExStyle = cp.ExStyle | WS_EX_LAYERED;
                return cp;
            }
        }

        private void SetLayeredWindow(Bitmap srcBitmap)
        {
            const byte AC_SRC_OVER = 0;
            const byte AC_SRC_ALPHA = 1;
            const int ULW_ALPHA = 2;

            // スクリーンの Graphics と、hdcを取得
            Graphics g_sc = Graphics.FromHwnd(IntPtr.Zero);
            IntPtr hdc_sc = g_sc.GetHdc();

            // BITMAP の Graphicsと、hdc を取得
            Graphics g_bmp = Graphics.FromImage(srcBitmap);
            IntPtr hdc_bmp = g_bmp.GetHdc();

            // BITMAP の hdc でサーフェイスの BITMAP を選択する
            // このとき背景を無色透明にしておく
            IntPtr oldhbmp = SelectObject(hdc_bmp, srcBitmap.GetHbitmap(Color.FromArgb(0)));

            // BLENDFUNCTION を初期化
            BLENDFUNCTION blend = new BLENDFUNCTION();
            blend.BlendOp = AC_SRC_OVER;
            blend.BlendFlags = 0;
            blend.SourceConstantAlpha = 255;
            blend.AlphaFormat = AC_SRC_ALPHA;

            // ウィンドウ位置の設定
            Point pos = new Point(this.Left, this.Top);

            // サーフェースサイズの設定
            Size surfaceSize = new Size(this.Width, this.Height);

            // サーフェース位置の設定
            Point surfacePos = new Point(0, 0);

            // レイヤードウィンドウの設定
            UpdateLayeredWindow(this.Handle, hdc_sc, ref pos, ref surfaceSize, hdc_bmp, ref surfacePos, 0, ref blend, ULW_ALPHA);

            // 後始末
            DeleteObject(SelectObject(hdc_bmp, oldhbmp));
            g_sc.ReleaseHdc(hdc_sc);
            g_sc.Dispose();
            g_bmp.ReleaseHdc(hdc_bmp);
            g_bmp.Dispose();
        }

        public Zunda(string[] args)
        {
            InitializeComponent();
            InitializePositionVelocity(((args == null) || (args.Length == 0)) ? "-5x-5+600+200" : args[0]);
        }

        private void InitializePositionVelocity(string positionVelocity)
        {
            var tokens = positionVelocity.Split(new char[] { 'x', '+' });
            if (tokens.Length != 4)
            {
                return;
            }

            position = new Point(int.Parse(tokens[2]), int.Parse(tokens[3]));

            const float ratio = 1.20f;
            velocity = new PointF(ratio * float.Parse(tokens[0]), ratio * float.Parse(tokens[1]));
        }

        private void Zunda_Load(object sender, EventArgs e)
        {
            InitializeForm();
            MoveZunda();
        }

        private void InitializeForm()
        {
            FormBorderStyle = FormBorderStyle.None;
            ShowInTaskbar = false;
            Cursor.Current = Cursors.Default;
            Size = ImageSize(ZundaImage);
        }

        private Size ImageSize(string path)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Bitmap image = new Bitmap(assembly.GetManifestResourceStream(path));
            return image.Size;
        }

        private void ShowImage(string path, int degree)
        {
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Bitmap image = new Bitmap(assembly.GetManifestResourceStream(path));
            Bitmap rotated = RotatedBitmap(image, degree);
            SetLayeredWindow(rotated);
            Refresh();
        }

        private Bitmap RotatedBitmap(Bitmap bmp, float angle)
        {
            Bitmap bmp2 = new Bitmap((int)bmp.Width, (int)bmp.Height);
            Graphics g = Graphics.FromImage(bmp2);

            g.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);
            g.RotateTransform(angle);
            g.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;

            g.DrawImageUnscaled(bmp, 0, 0);
            g.Dispose();

            return bmp2;
        }

        private Rectangle WindowSize(Point point)
        {
            Screen screen = Screen.FromPoint(point);
            return screen.Bounds;
        }

        private void WaitSeconds(double second)
        {
            var task = Task.Factory.StartNew(() =>
            {
                Thread.Sleep((int)(1000 * second));
            });
            task.Wait();
        }

        private void MoveZunda()
        {
            var random = new Random();
            int rotation = random.Next(0, 359);
            int rotationStep = random.Next(-40, +40);

            Point windowCenter = WindowCenter();
            position.X += windowCenter.X;
            position.Y += windowCenter.Y;

            // 実行遅延を考慮して、少し位置を進める
            for (int i = 0; i < 4; ++i)
            {
                updatePosition();
            }
            
            while (true)
            {
                Point pos = new Point((int)position.X, (int)position.Y);
                Location = pos;
                ShowImage(ZundaImage, rotation);
                WaitSeconds(0.025);

                updatePosition();
                rotation += rotationStep;

                //画面下まで移動したら終了する
                var window = WindowSize(pos);
                if (pos.Y > window.Bottom)
                {
                    Application.Exit();
                    return;
                }
            }
        }

        private void updatePosition()
        {
            position.X += velocity.X;
            position.Y += velocity.Y;
            velocity.Y += 1;
        }

        private Point WindowCenter()
        {
            Process[] ps = Process.GetProcessesByName("ZundaClicker");
            foreach (Process p in ps)
            {
                if (IsWindowVisible(p.MainWindowHandle))
                {
                    RECT rect = new RECT();
                    GetWindowRect(p.MainWindowHandle, ref rect);
                    return new Point((rect.left + rect.right) / 2, (rect.top + rect.bottom) / 2);
                }
            }

            // 画面外になる値を返す
            position.Y = Screen.PrimaryScreen.Bounds.Height * 3;
            return new Point(0, Screen.PrimaryScreen.Bounds.Height * 3);
        }
    }
}
