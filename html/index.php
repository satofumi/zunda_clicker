<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="default.css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="icon" type="image/png" href="zunda_favicon.png">
<title>ずんだ餅クリッカー</title>
</head>
<body>
<br class="short">
<div id="wrapper">
<br>
<img src="zzm_minizunda.png" align="right">

<h1>ずんだ餅クリッカー</h1>
<div class="section">
東北ずん子たち３姉妹による、ほのぼの癒やし系クリッカーゲームです。<br>
皆さんがこのアプリを遊んで笑顔になるといいな、と思いながら頑張って作りました。<br>
20 分くらいでクリアできるように作ったので、ちょっとした時間に遊んでみてくださいね。<br>
<br>
追記:<br>
<div class="section">
上記の記述は本心ではありませんでした。 ごめんなさい。<br>
本当は遊んだ人が<strong>「このアプリ頭おかしい！」</strong>って思ったり、<strong>思わず吹き出してくれること</strong>を期待して、思いついたネタを盛り込んで作ったアプリです。
</div>
</div>
<br clear="right">


<h2>どんなゲームなの？</h2>
<div class="section">
クリックしてずんだ餅を落とす、そんな感じのゲームです。<br>

<table>
  <tr><td>
    <img src="zunda_clogged.png" class="screenshot"><br>
    ずんだ餅は、ときどき詰まってしまいますが...<br>
  </td><td>
    <img src="zunda_free.png" class="screenshot"><br>
    クリックすると落ち始めます！<br>
  </td></tr>
  <tr><td>
    <img src="zzm_kiritan_nabe.png">
  </td><td>
    <img src="kiritan_hou.png" class="screenshot"><br>
    ゲームが進むと、ずん子たちのスキルで<br>ずんだ餅の詰まりを解消できるようになります。
  </td></tr>
</table>
<br clear="right">
</div>


<img src="zzm_itako_tea.png" align="right">
<h2>ダウンロード</h2>
<div class="section">

<h3>最新バージョン</h3>
  <ul>
    <li><a href="http://hyakuren-soft.sakura.ne.jp/ZundaClicker/packages/ZundaClicker-1.0.0.zip">ZundaClicker-1.0.0.zip</a></li>
  </ul>

<h3>動作環境</h3>
  <ul>
    <li>Windows 7 以降</li>
  </ul>

<h3>ライセンス</h3>
  <ul>
    <li>MIT License</li>
  </ul>
</div>


<h2>連絡先</h2>
<div class="section">
バグや要望、このアプリがもっとシュールになる案を思いついた方は私にお知らせください。<br>
  <br>

  <ul>
    <li>Twitter: <strong>@satofumi_</strong></li>
    <li>e-mail: <a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;%73%61%74%6f%66%75%6d%69@%75%73%65%72%73.%73%6f%75%72%63%65%66%6f%72%67%65.%6a%70" class="mail">&#x73;&#x61;&#x74;&#x6f;&#x66;&#x75;&#x6d;&#x69;@&#x75;&#x73;&#x65;&#x72;&#x73;.&#x73;&#x6f;&#x75;&#x72;&#x63;&#x65;&#x66;&#x6f;&#x72;&#x67;&#x65;.&#x6a;&#x70;</a><br /></li>
    <li>開発サイト: <a href="https://bitbucket.org/satofumi/zunda_clicker">https://bitbucket.org/satofumi/zunda_clicker</a></li>
  </ul>
<br clear="right">
</div>


<h2>これまでに寄付されたずんだ餅の数</h2>
<div class="section">
<?php
require_once('ZundaDB.php');

$db = new ZundaDB();
$db->open();
$count = $db->total_zunda_count(urldecode($_GET['key']));
$db->close();

print(number_format($count) . ' 個！');
?>
<br>本当にありがとうございます！
</div>


<!-- <h2>よくある質問</h2>
<div class="section">
Q. ずん子がずんだ餅を食べると Zunda カウントが増えるのはなぜですか？<br>
A. ずん子の胃袋と穴の先とが胃空間でつながっているからです。<br>

Q.「もちもちジャンプ」中にずん子がジャンプするのはなぜですか？<br>
A. 息継ぎのためです。<br>
</div> -->

<div id="footer">
背景素材: &nbsp;<a href="http://free-texture.net/">http://free-texture.net/</a><br>
キャラクター素材: &nbsp;<a href="http://zunko.jp/">http://zunko.jp/</a><br>
</div>
<br>
</div>
<br>
</body>
</html>
