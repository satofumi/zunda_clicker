<?php
require_once('config.php');

class ZundaDB
{
    var $db;

    public function open()
    {
        $this->db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if (!$this->db) {
            return false;
        }

        return true;
    }

    public function close()
    {
        $this->db->close();
        $this->db = null;
    }

    public function total_zunda_count($key)
    {
        $query = "SELECT SUM(zunda_count) FROM DonateData";
        $result = $this->db->query($query);
        if (!$result) {
            return -1;
        }

	$record = $result->fetch_array(MYSQLI_NUM);
        return intval($record[0]);
    }

    public function create_register_key()
    {
        define('KEY_LEN', 64);
        $retry_times = 10;
        for ($i = 0; $i < $retry_times; ++$i) {
	    $binary_key = openssl_random_pseudo_bytes(KEY_LEN);
            $key = substr(base64_encode($binary_key), 0, KEY_LEN);
            $query = "INSERT INTO RegisterKeys (register_key, date_time) VALUES ('$key', cast(now() as datetime))";
            $result = $this->db->query($query);
            if ($result) {
                return $key;
            }
        }

        return false;
    }

    public function donate_zunda($key, $count)
    {
        $query = "INSERT INTO DonateData (register_key, date_time, zunda_count) VALUES ('$key', cast(now() as datetime), $count)";
        $result = $this->db->query($query);
	if (!$result) {
	    return false;
	}
	return true;
    }
}
?>
