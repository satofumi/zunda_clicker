CREATE TABLE RegisterKeys (
  register_key VARCHAR(64) NOT NULL PRIMARY KEY,
  date_time DATETIME NOT NULL,
  active BOOLEAN DEFAULT TRUE
);

CREATE TABLE DonateData (
  register_key VARCHAR(64) NOT NULL,
  date_time DATETIME NOT NULL,
  zunda_count INT NOT NULL,

  FOREIGN KEY (register_key) REFERENCES RegisterKeys(register_key)
);
