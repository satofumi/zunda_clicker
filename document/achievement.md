# 実績

## 実績解除の条件

- zunda の個数


## 効果

- 背景画像を配置する。

- ずん子を配置する。
- きりたんを配置する。
- イタコを配置する。

- 「きりたん砲」が使えるようになります。
- 「ずんだ口寄せ」が使えるようになります。
- 「ずんだアロー」が使えるようになります。

- 各スキルにカットイン演出がつきます。

- 「もちもちドロップ」が使えるようになります。

- 「ずんだ口寄せ」の時間が長くなります。(召喚エフェクトがつく)
- 「ずんだアロー」の威力が増します。(対象にする餅の数が増える)
- 「きりたん砲」の威力が増します。(爆発エフェクトがつく)

- ずんだ餅カットインの文字列を変更する。

- ゲームクリア
  - クリア画像を表示する。
  - ずんだ餅の数をサーバに登録できるようになる。