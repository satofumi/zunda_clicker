UNITY_EXE = /C/Program\ Files/Unity/Hub/Editor/2019.4.7f1/Editor/Unity.exe
MSBUILD_EXE = /C/Windows/Microsoft.NET/Framework/v4.0.30319/MSBuild.exe

WALKING_ZUNKO_EXE = WalkingZunko/WalkingZunko/bin/Release/WalkingZunko.exe
FALLING_ZUNDA_EXE = FallingZunda/FallingZunda/bin/Release/FallingZunda.exe
VERSION = `cat ChangeLog.txt | awk "NR==2,NR==2 {print}" | awk '$$0 = substr($$0, 5)'`
PACKAGE_DIR = ZundaClicker-$(VERSION)

all :
	$(UNITY_EXE) -batchmode -nographics -projectPath $(PWD)/ZundaClicker/ -buildWindowsPlayer $(PWD)/ZundaClicker.exe -quit
	$(MSBUILD_EXE) -p:Configuration=Release WalkingZunko/WalkingZunko.sln
	cp $(WALKING_ZUNKO_EXE) ZundaClicker_Data/
	$(MSBUILD_EXE) -p:Configuration=Release FallingZunda/FallingZunda.sln
	cp $(FALLING_ZUNDA_EXE) ZundaClicker_Data/
	$(RM) -rf $(PACKAGE_DIR)
	mkdir -p $(PACKAGE_DIR)
	mv ZundaClicker.exe ZundaClicker_Data $(PACKAGE_DIR)
	mv UnityPlayer.dll MonoBleedingEdge $(PACKAGE_DIR)/
	cp README.md $(PACKAGE_DIR)/README.txt
	cp LICENSE.txt ChangeLog.txt $(PACKAGE_DIR)/
	zip -r $(PACKAGE_DIR).zip $(PACKAGE_DIR)/

clean :

upload :
	rsync -avz -e ssh --delete html/*.php html/*.css html/*.png hyakuren-soft@hyakuren-soft.sakura.ne.jp:/home/hyakuren-soft/www/ZundaClicker/
	rsync -avz -e ssh --delete html/packages/*.zip hyakuren-soft@hyakuren-soft.sakura.ne.jp:/home/hyakuren-soft/www/ZundaClicker/packages/
